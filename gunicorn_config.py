# 多线程
import multiprocessing

# 绑定ip+端口号
bind = '0.0.0.0:1433'
# 进程数 = cpu数量*2+1
# workers = multiprocessing.cpu_count() * 2 + 1
# 工作模式-协程 采用gevent库 支持异步处理请求 提高吞吐量
worker_class = 'gevent'
# 最大客户端并发数量
# worker_connections = 1000
# # 进程名称
# proc_name = 'gunicorn.pid'
# # 进程pid记录文件
# pidfile = 'gunicorn.pid'
# # 日志等级
# loglevel = 'warning'
# # 日志文件名
# logfile = 'gunicorn_log.log'
# # 设置访问日志
# accesslog = 'gunicorn_error.log'
# # 设置错误信息日志
# errorlog = 'gunicorn_error.log'
# 代码发生变化是否自动重启
reload = True
