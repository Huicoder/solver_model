from queue import Queue  # noqa
import sys
import requests
sys.path.append('/root/solver_model')
from interface.utils.preprocessing import preprocessing
from interface.utils.postprocessing import postprocessing
from components.packages.platgo.algorithms import DE, GA, PSO,NSGA2  # noqa
from components.packages.common.commons import AlgoResultType

def process_data(data_set):
    """
    入口函数
    :param data_set: 数据集
    :return:
    """
    (
        lower,
        upper,
        n_var,
        task_info_dict,
        resources_info_dict,
        resource_tasks_dict,
        individual,
        task_time_lines_taskid_dict
    ) = preprocessing(data_set)

    optimization_problem = {
        "name": "RCPSP_Single",
        "n_var": n_var,
        "lower": lower,
        "upper": upper,
        "task_info_dict": task_info_dict,
        "resources_info_dict": resources_info_dict,
        "resource_tasks_dict": resource_tasks_dict,
        "individual": individual,
        "dataSet": [data_set],
        "algoResultType": 0,
    }

    pop_size = 100
    max_fe = 500
    options = {}
    simulation_request_callback = None

    evol_algo = GA(
        pop_size=pop_size,
        options=options,
        optimization_problem=optimization_problem,
        control_cb=None,  # noqa
        max_fe=max_fe,
        name="GA-Thread",
        debug=True,
    )
    evol_algo.start()
    evol_algo.join()
    algo_result = evol_algo.get_external_algo_result()
    objv_max, return_body = postprocessing(data_set, algo_result, task_info_dict, resources_info_dict, task_time_lines_taskid_dict)
    return objv_max, return_body


def process_data_with_obj(data_set):
    """
    入口函数
    :param data_set: 数据集
    :return:
    """
    (
        lower,
        upper,
        n_var,
        task_info_dict,
        resources_info_dict,
        resource_tasks_dict,
        individual,
        task_time_lines_taskid_dict
    ) = preprocessing(data_set)

    optimization_problem = {
        "name": "RCPSP_Multi",
        "n_obj": len(data_set["ObjectiveFunction"]) + 1,
        "n_var": n_var,
        "lower": lower,
        "upper": upper,
        "objective_function": data_set["ObjectiveFunction"],
        "task_info_dict": task_info_dict,
        "resources_info_dict": resources_info_dict,
        "resource_tasks_dict": resource_tasks_dict,
        "individual": individual,
        "dataSet": [data_set],
        "algoResultType": 0,
    }

    pop_size = 100
    max_fe = 500
    options = {}
    simulation_request_callback = None

    evol_algo = NSGA2(
        pop_size=pop_size,
        options=options,
        optimization_problem=optimization_problem,
        control_cb=None,  # noqa
        max_fe=max_fe,
        name='NSGA2-Thread',
        debug=True,
    )
    evol_algo.set_data_type(AlgoResultType.HV)
    evol_algo.start()
    evol_algo.join()
    algo_result = evol_algo.get_external_algo_result()
    objv_max, return_body = postprocessing(data_set, algo_result, task_info_dict, resources_info_dict,
                                           task_time_lines_taskid_dict)
    return objv_max, return_body


def main(data):
    # 对数据进行处理
    # 无目标函数
    if ("ObjectiveFunction" in data and len(data["ObjectiveFunction"])==0) or "ObjectiveFunction" not in data:
        _, result = process_data(data)
    else:  # 有目标函数
        _, result = process_data_with_obj(data)
    # 处理完毕后，准备返回的数据
    response_data = result
    print("+++++++++++++++++")
    print(response_data)
    print("+++++++++++++++++")
    url = data["NotifyUrl"]
    headers = {
        'Content-Type': 'application/json'
    }
    response = requests.post(url, json=response_data, headers=headers)
    print(response)





