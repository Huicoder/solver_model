import json
from queue import Queue  # noqa
import sys
import threading
import logging
sys.path.append('/root/solver_model')
from interface import checkConflict, checkConflictFixed, toSolve
from components.packages.platgo.algorithms import DE, GA, PSO  # noqa
from flask import Flask, request, jsonify

app = Flask(__name__)


@app.route('/checkConflict', methods=['POST'])
def handle_json_check_conflict():
    if request.is_json:
        data = request.get_json()
        print("+++++++++++++checkConflict+++++++++++++++++")
        print(data)
        print("+++++++++++++checkConflict+++++++++++++++++")
        # with open(f"/root/solver_model/log/{data['SceneId']}_{data['SceneName']}.json", "w",
        #           encoding="UTF-8", ) as file:
        #     json.dump(data, file, ensure_ascii=False)
        # 对数据进行处理
        result = checkConflict.main(data)
        # 处理完毕后，准备返回的数据
        response_data = result
        return jsonify(response_data)
    else:
        return jsonify({'status': 'error', 'message': '请求需要JSON数据'}), 400


@app.route('/checkConflictFixed', methods=['POST'])
def handle_json_check_conflict_fixed():
    if request.is_json:
        data = request.get_json()
        print("+++++++++++++checkConflictFixed+++++++++++++++++")
        print(data)
        print("+++++++++++++checkConflictFixed+++++++++++++++++")
        # with open(f"/root/solver_model/log/{data['SceneId']}_{data['SceneName']}.json", "w",
        #           encoding="UTF-8", ) as file:
        #     json.dump(data, file, ensure_ascii=False)
        # 对数据进行处理
        result = checkConflictFixed.main(data)
        # 处理完毕后，准备返回的数据
        response_data = result
        return jsonify(response_data)
    else:
        return jsonify({'status': 'error', 'message': '请求需要JSON数据'}), 400


@app.route('/toSolve', methods=['POST'])
def handle_json_to_solve():
    if request.is_json:
        data = request.get_json()
        print("+++++++++++++checkConflictFixed+++++++++++++++++")
        print(data)
        print("+++++++++++++checkConflictFixed+++++++++++++++++")
        try:
            thread = threading.Thread(target=toSolve.main, args=(data,))
            thread.start()
            return jsonify({'Name': 'success', 'Code': 200})
        except Exception as e:
            return jsonify({'Name': 'fail', 'Code': 500})
    else:
        return jsonify({'status': 'error', 'message': '请求需要JSON数据'}), 400


if __name__ == '__main__':
    app.config['JSON_AS_ASCII'] = False
    app.config['JSONIFY_MIMETYPE'] = "application/json;charset=UTF-8"
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.run(host="0.0.0.0")
