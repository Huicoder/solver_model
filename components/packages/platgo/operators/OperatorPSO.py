import numpy as np
from ..Population import Population


"""
 OperatorPSO - The operator of particle swarm optimization.
   Off = OperatorPSO(P,Pbest,Gbest) uses the operator of particle swarm
   optimization to generate offsprings based on particles P, personal best
   particles Pbest, and global best particles Gbest. P, Pbest, and Gbest
   should be arrays of Population objects, and Off is also an array of
   Population objects. Each object of P, Pbest, and Gbest is used to
   generate one offspring.

   Off = OperatorPSO(P,Pbest,Gbest,W) specifies the parameter of the
   operator, where W is the inertia weight.

   Example:
       Off = OperatorPSO(Population,Pbest,Gbest)

 ------------------------------------ Reference -------------------------------
 C. A. Coello Coello and M. S. Lechuga, MOPSO: A proposal for multiple
 objective particle swarm optimization, Proceedings of the IEEE Congress
 on Evolutionary Computation, 2002, 1051-1056.
 ------------------------------------------------------------------------------
"""


def OperatorPSO(pop, pbest, gbest, w):
    if w is None:
        w = .4
    pbest_dec = pbest.decs
    gbest_dec = gbest.decs
    N, D = pop.decs.shape
    if pop.vel is None:
        pop.vel = np.zeros((N, D))

    r1 = np.random.random((N, 1))
    r2 = np.random.random((N, 1))
    offvel = w * pop.vel + r1 * (pbest_dec - pop.decs) + r2 * (gbest_dec - pop.decs)
    offdec = pop.decs + offvel
    offspring = Population(decs=offdec, vel=offvel)

    return offspring
