import datetime
import copy
def preprocessing(data_set):
    """
    数据预处理
    :param data_set:
    :return: 决策向量的上下界，决策向量的长度
    """
    tasks = data_set["TaskTimeLines"]
    resources = data_set["Resources"]
    task_info_dict = dict()  # 存储任务的信息
    resources_info_dict = dict()  # 存储资源的信息
    resource_tasks_dict = dict()  # 存储每个资源被哪些任务所使用
    task_time_lines_taskid_dict = dict()  # 存储每一条任务时间线中包含的任务ID
    lower = list()
    upper = list()
    n_var = 0
    individual = list()  # 期望时间作为初始种群中的一个解
    time_format = "%Y-%m-%dT%H:%M:%S"
    # 存储资源的信息
    if len(resources) != 0:
        for item2 in resources:
            # TimeLimits = list()
            # if len(item2["TimeLimits"]) == 0:  # 用户没有添加资源限制，这个资源在任何时候都可以使用且数量无穷大
            #     TimeLimits.append({"BeginTime": "2000-01-01T00:00:00",
            #                        "EndTime": "3000-01-01T00:00:00",
            #                        "MaxValue": pow(2, 31) - 1
            #                        })
            # else:  # 用户添加了资源限制
            #     pre_begin_time = item2["TimeLimits"][0]["BeginTime"]
            #     for item3 in item2["TimeLimits"]:
            #         if pre_begin_time == item3["BeginTime"]:
            #             TimeLimits.append({"BeginTime": item3["BeginTime"],
            #                                "EndTime": item3["EndTime"],
            #                                "MaxValue": item3["MaxValue"]
            #                                })
            #         else:
            #             TimeLimits.append({"BeginTime": pre_begin_time,
            #                                "EndTime": item3["BeginTime"],
            #                                "MaxValue": 0.0
            #                                })
            #             TimeLimits.append({"BeginTime": item3["BeginTime"],
            #                                "EndTime": item3["EndTime"],
            #                                "MaxValue": item3["MaxValue"]
            #                                })
            #         pre_begin_time = item3["EndTime"]
            # item2["TimeLimits"] = TimeLimits
            resources_info_dict[item2["ResourceId"]] = {
                "Name": item2["Name"],
                "IsShare": item2["IsShare"],
                "IsReuse": item2["IsReuse"],
                "TimeLimits": item2["TimeLimits"]
            }

    # 对任务进行遍历
    for i in range(len(tasks)):  # 对任务线进行遍历
        if len(tasks[i]["Tasks"]) != 0:  # 任务集合中有任务
            for item in tasks[i]["Tasks"]:  # 对任务集合进行遍历
                early_start_date = item["EarlyStartDate"]
                late_finish_date = item["LateFinishDate"]
                # 设置决策变量的上下界
                if item["ExpectStartTime"] and not item["ExpectFinishTime"]:  # 期望开始时间不为空 期望结束时间为空
                    expect_start_time = item["ExpectStartTime"]
                    l_b = (datetime.datetime.strptime(expect_start_time, time_format) - datetime.datetime.strptime(
                        early_start_date, time_format)).total_seconds()
                    need_times = item["NeedTimes"]
                    all_seconds = (
                                datetime.datetime.strptime(late_finish_date, time_format) - datetime.datetime.strptime(
                            early_start_date, time_format)).total_seconds()
                    r_b = all_seconds - need_times
                elif not item["ExpectStartTime"] and item["ExpectFinishTime"]:  # 期望结束时间不为空 期望开始时间为空
                    l_b = 0
                    need_times = item["NeedTimes"]
                    expect_finish_time = item["ExpectFinishTime"]
                    expect_start_time = (datetime.datetime.strptime(expect_finish_time,
                                                                    "%Y-%m-%dT%H:%M:%S") - datetime.timedelta(
                        seconds=need_times)).strftime("%Y-%m-%dT%H:%M:%S")
                    all_seconds = (datetime.datetime.strptime(expect_finish_time,
                                                              time_format) - datetime.datetime.strptime(
                        early_start_date, time_format)).total_seconds()
                    r_b = all_seconds - need_times
                elif not item["ExpectStartTime"] and not item["ExpectFinishTime"]:  # 期望结束时间为空 期望开始时间为空
                    l_b = 0
                    expect_start_time = item["EarlyStartDate"]
                    need_times = item["NeedTimes"]
                    all_seconds = (
                                datetime.datetime.strptime(late_finish_date, time_format) - datetime.datetime.strptime(
                            early_start_date, time_format)).total_seconds()
                    r_b = all_seconds - need_times
                else:  # 期望开始时间 期望结束时间均不为空
                    l_b = 0
                    expect_start_time = item["ExpectStartTime"]
                    expect_finish_time = item["ExpectFinishTime"]
                    need_times = (datetime.datetime.strptime(expect_finish_time,
                                                             time_format) - datetime.datetime.strptime(
                        expect_start_time, time_format)).total_seconds()
                    all_seconds = (
                                datetime.datetime.strptime(late_finish_date, time_format) - datetime.datetime.strptime(
                            early_start_date, time_format)).total_seconds()
                    r_b = all_seconds - need_times
                lower.append(l_b)
                upper.append(r_b + 1)
                n_var += 1
                individual.append(
                    (datetime.datetime.strptime(expect_start_time, time_format) - datetime.datetime.strptime(
                        early_start_date, time_format)).total_seconds())
                # 存储任务的信息
                item_copy = copy.deepcopy(item)
                del item_copy["TaskId"]
                task_info_dict[item["TaskId"]] = item_copy
                # 存储资源被哪些任务使用
                if len(item["ResourceUsages"]) != 0:
                    for item2 in item["ResourceUsages"]:
                        resource_tasks_dict.setdefault(item2["ResourceId"], {}).setdefault(item["TaskId"],
                                                                                           item2["Items"])
                task_time_lines_taskid_dict.setdefault(tasks[i]["TimeLineId"], []).append(item["TaskId"])
    return (
        lower,
        upper,
        n_var,
        task_info_dict,
        resources_info_dict,
        resource_tasks_dict,
        individual,
        task_time_lines_taskid_dict
    )


def preprocessing_fixed(data_set):
    """
    数据预处理
    :param data_set:
    :return:
    """
    tasks = data_set["TaskTimeLines"]
    resources = data_set["Resources"]
    task_info_dict = dict()  # 存储任务的信息
    resources_info_dict = dict()  # 存储资源的信息
    resource_tasks_dict = dict()  # 存储每个资源被哪些任务所使用
    task_time_lines_taskid_dict = dict()
    n_var = 0
    if len(resources) != 0:
        for item2 in resources:
            # TimeLimits = list()
            # if len(item2["TimeLimits"]) == 0:  # 用户没有添加资源限制，这个资源在任何时候都可以使用且数量无穷大
            #     TimeLimits.append({"BeginTime": "2000-01-01T00:00:00",
            #                        "EndTime": "3000-01-01T00:00:00",
            #                        "MaxValue": pow(2, 31) - 1
            #                        })
            # else:  # 用户添加了资源限制
            #     pre_begin_time = item2["TimeLimits"][0]["BeginTime"]
            #     for item3 in item2["TimeLimits"]:
            #         if pre_begin_time == item3["BeginTime"]:
            #             TimeLimits.append({"BeginTime": item3["BeginTime"],
            #                                "EndTime": item3["EndTime"],
            #                                "MaxValue": item3["MaxValue"]
            #                                })
            #         else:
            #             TimeLimits.append({"BeginTime": pre_begin_time,
            #                                "EndTime": item3["BeginTime"],
            #                                "MaxValue": 0.0
            #                                })
            #             TimeLimits.append({"BeginTime": item3["BeginTime"],
            #                                "EndTime": item3["EndTime"],
            #                                "MaxValue": item3["MaxValue"]
            #                                })
            #         pre_begin_time = item3["EndTime"]
            # item2["TimeLimits"] = TimeLimits
            resources_info_dict[item2["ResourceId"]] = {
                "Name": item2["Name"],
                "IsShare": item2["IsShare"],
                "IsReuse": item2["IsReuse"],
                "TimeLimits": item2["TimeLimits"]
            }

    for i in range(len(tasks)):
        if len(tasks[i]["Tasks"]) != 0:
            for item in tasks[i]["Tasks"]:
                n_var += 1
                task_info_dict[item["TaskId"]] = {
                    "Name": item["Name"],
                    "NeedTimes": item["NeedTimes"],
                    "RealStart": item["RealStart"],
                    "RealFinish": item["RealFinish"],
                    "ResourceUsages": item["ResourceUsages"]
                }
                if len(item["ResourceUsages"]) != 0:
                    for item2 in item["ResourceUsages"]:
                        resource_tasks_dict.setdefault(item2["ResourceId"], {}).setdefault(item["TaskId"],
                                                                                           item2["Items"])
                task_time_lines_taskid_dict.setdefault(tasks[i]["TimeLineId"], []).append(item["TaskId"])
    return (
        n_var,
        task_info_dict,
        resources_info_dict,
        resource_tasks_dict,
        task_time_lines_taskid_dict
    )
