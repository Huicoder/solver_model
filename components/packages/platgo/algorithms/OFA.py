import numpy as np
from .. import GeneticAlgorithm
from ..Population import Population


class OFA(GeneticAlgorithm):
    type = {
        "n_obj": "single",
        "encoding": "real",
        "special": {"constrained/none", "large/none"},
    }

    def __init__(
        self,
        pop_size,
        options,
        optimization_problem,
        control_cb,
        max_fe=10000,
        name="OFA",
        show_bar=False,
        sim_req_cb=None,
        debug=False,
    ):
        super(OFA, self).__init__(
            pop_size,
            options,
            optimization_problem,
            control_cb,
            max_fe=max_fe,
            name=name,
            show_bar=show_bar,
            sim_req_cb=sim_req_cb,
            debug=debug,
        )

    def run_algorithm(self):
        """
         main function for Different Evolution
         if population is None, generate a new population with N
        :param N: population size
        :param population: population to be optimized
        :return:
        """
        pop = self.problem.init_pop()
        self.cal_obj(pop)
        rank = np.argsort(self.fitness_single(pop)[:, 0])
        pop = pop[rank]

        while self.not_terminal(pop):
            PopDec = pop.decs.copy()
            # PopDec([end,floor(unifrnd(ones(1,end-1),2:end))],:)
            a = np.ones((1, PopDec.shape[0] - 1))
            c = np.arange(2, PopDec.shape[0] + 1)
            c = np.reshape(c, (1, -1))
            e = np.zeros((1, PopDec.shape[0] - 1))
            for i in range(PopDec.shape[0] - 1):
                e[0, i] = np.floor(np.random.uniform(a[0, i], c[0, i])) - 1
            f = np.insert(e[0], 0, PopDec.shape[0] - 1)
            h = PopDec[f.astype("int64")]

            OffDec = PopDec + self._gen / self._max_fe * (
                np.random.rand(np.shape(PopDec)[0], np.shape(PopDec)[1])
                - np.random.rand(np.shape(PopDec)[0], np.shape(PopDec)[1])
            ) * (PopDec - h)

            Offspring = Population(decs=OffDec)
            self.cal_obj(Offspring)

            lambda1 = np.random.rand(pop.pop_size, 1)

            replace = lambda1 * np.reshape(
                self.fitness_single(Offspring)[:, 0], (-1, 1)
            ) / (
                1 + lambda1 * np.ceil(self._gen / self.problem.pop_size)
            ) < np.reshape(
                self.fitness_single(pop)[:, 0], (-1, 1)
            ) / np.ceil(
                self._gen / self.problem.pop_size
            )

            pop[replace[:, 0]] = Offspring[replace[:, 0]]
            rank = np.argsort(self.fitness_single(pop)[:, 0])
            pop = pop[rank]

        return pop

    def fitness_single(self, pop: Population):
        pop_con = np.sum(np.where(pop.cv < 0, 0, pop.cv), axis=1)
        feasible = pop_con <= 0
        fitness = feasible * pop.objv + ~feasible * np.all(pop_con + 1e10)
        return fitness
