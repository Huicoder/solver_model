import copy
import datetime
import networkx as nx
from queue import Queue  # noqa
import sys
sys.path.append('/root/solver_model')
from components.packages.platgo.algorithms import DE, GA, PSO  # noqa
from interface.utils.preprocessing import preprocessing
from interface.utils.common import *

# 查找所有简单环，检查是否存在满足条件的循环依赖
# 检测严格的循环依赖，并返回构成循环的任务
# def check_strict_cycle_with_tasks(data_set, task_info_dict):
#     """
#     查找所有简单环，检查是否存在满足条件的循环依赖
#     检测严格的循环依赖，并返回构成循环的任务
#     :param data_set:
#     :param task_info_dict:
#     :return:
#     """
#     error_all = list()
#     task_list = list()
#     # 前置任务和后续任务之间的依赖关系
#     strict_cycle_exists = dict()
#     for item in data_set["TaskTimeLines"]:
#         dependencies = item["TaskLimits"]
#         # 创建一个有向图
#         G = nx.DiGraph()
#
#         # 添加边，同时保存关系的属性
#         for dep in dependencies:
#             G.add_edge(dep['BeforeTaskId'], dep['AfterTaskId'], BeforeTimeType=dep['BeforeTimeType'],
#                        AfterTimeType=dep['AfterTimeType'], Compare=dep['Compare'])
#         for cycle in nx.simple_cycles(G):
#             # 获取环中的边属性
#             cycle_edges = [(G[u][v]['BeforeTimeType'], G[u][v]['AfterTimeType']) for u, v in
#                            zip(cycle, cycle[1:] + cycle[:1])]
#
#             # 检查是否所有BeforeTimeType和AfterTimeType都相同
#             all_same = all(bt == cycle_edges[0][0] and at == cycle_edges[0][1] for bt, at in cycle_edges)
#
#             # 或者检查BeforeTimeType和AfterTimeType是否形成顺序变化
#             sequential_change = all(
#                 (cycle_edges[i][1] + 1) % 3 == cycle_edges[(i + 1) % len(cycle_edges)][0] for i in
#                 range(len(cycle_edges)))
#
#             if all_same or sequential_change:  # 存在符合严格条件的循环依赖
#                 if len(cycle) != 0:
#                     strict_cycle_exists[item["TimeLineId"]] = cycle
#                     cycle_list_name = list()
#                     error_ = dict()
#                     for cy in cycle:
#                         cycle_list_name.append(task_info_dict[cy]["Name"])
#                         task_list.append({"TaskId": cy, "TaskName": task_info_dict[cy]["Name"]})
#                     error_["Tasks"] = [{"Id": list(i)[0], "Name": list(i)[1]} for i in
#                                        list(zip(cycle, cycle_list_name))]
#                     error_["Resources"] = []
#                     error_["Type"] = 1
#                     error_["Name"] = f'【{",".join(f"{i}" for i in cycle_list_name)}】之间出现了循环冲突'
#                     error_["Value"] = ""
#                     error_["Suggestion"] = ""
#                     error_all.append(error_)
#     return error_all
#

def time_constrained_check(data_set, task_info_dict):
    """
    任务最早开始时间和最晚结束约束检查
    :return:
    """
    task_id_list = list(task_info_dict.keys())
    error_list = list()
    # Type-> 0：实际开始时间 RealStart 1：实际结束时间 RealFinish 2：最早开始时间 EarlyStartDate 3：最迟结束时间 LateFinishDate
    # Compare-> 0：等于 1：大于 2：大于等于 3：小于 4：小于等于
    for task_id in task_id_list:  # 对每个任务进行遍历
        for i in range(len(data_set["TaskTimeLines"])):
            task_limits = data_set["TaskTimeLines"][i]["TaskLimits"]
            for item in task_limits:  # 对约束列表进行遍历
                before_task_id = item["BeforeTaskId"]
                if task_id == before_task_id:  # 找到对每个任务的约束
                    before_time_type = item["BeforeTimeType"]
                    after_task_id = item["AfterTaskId"]
                    after_time_type = item["AfterTimeType"]
                    before_task_name = task_info_dict[before_task_id]["Name"]
                    after_task_name = task_info_dict[after_task_id]["Name"]
                    compare = item["Compare"]
                    value = item["Value"]
                    description, before_time, before_time_relative = get_time_by_type(before_task_id, before_time_type, task_info_dict)
                    description2, after_time, after_time_relative = get_time_by_type(after_task_id, after_time_type, task_info_dict)
                    real_value = (datetime.datetime.strptime(after_time,
                                                             "%Y-%m-%dT%H:%M:%S") - datetime.datetime.strptime(
                        before_time, "%Y-%m-%dT%H:%M:%S")).total_seconds()
                    if compare == 0:
                        operator_str = "!="
                    elif compare == 1:
                        operator_str = "<="
                    elif compare == 2:
                        operator_str = "<"
                    elif compare == 3:
                        operator_str = ">="
                    else:
                        operator_str = ">"
                    if eval(f'{real_value} {operator_str} {value}'):
                        error_ = dict()
                        error_["Tasks"] = [{"Id": before_task_id, "Name": task_info_dict[before_task_id]["Name"]},
                                           {"Id": after_task_id, "Name": task_info_dict[after_task_id]["Name"]}]
                        error_["Resources"] = []
                        error_[
                            "Name"] = f'前置【{before_task_name}】的【{description}】与后置【{after_task_name}】的【{description2}】冲突'
                        error_["Value"] = real_value
                        error_["Type"] = get_type(description, description2)
                        before_time_list = [before_time, before_time_relative]
                        after_time_list = [after_time, after_time_relative]
                        error_["Suggestion"] = get_suggestion(operator_str, description, description2, value,
                                                              real_value, before_task_name, after_task_name,task_info_dict,before_task_id,after_task_id,before_time_list,after_time_list)
                        error_list.append(error_)
    return error_list


def resource_constrained_check(resource_tasks_dict: dict, resources_info_dict: dict, task_info_dict: dict, ):
    """
    资源时间段和任务时间段交集与使用量
    :param resources_info_dict:
    :param resource_tasks_dict:
    :param task_info_dict:
    :return: 划分后的区间列表，包括原区间和所有划分来源。
    """
    error_all_list = list()
    # 对每种资源进行遍历
    for key, value in resource_tasks_dict.items():
        # 找出该资源的所有的可用时间段
        r_time_limits_list = list()
        resources_info_dict = copy.deepcopy(resources_info_dict)
        # 共享资源（true）：多个任务可同时使用；独占资源（false）：只能有一个任务同时使用
        is_share = resources_info_dict[key]["IsShare"]
        # 重用资源（true）：可以重复使用；消耗资源（false）：使用1次后就没有了
        is_reuse = resources_info_dict[key]["IsReuse"]
        for item in resources_info_dict[key]["TimeLimits"]:
            r_time_limits_list.append([item["BeginTime"], item["EndTime"], item["MaxValue"]])
        # 找出使用该资源的所有的任务
        all_task_use_resource_dict = dict()
        for t_id in list(resource_tasks_dict[key].keys()):
            split_early_start_time = (datetime.datetime.strptime(task_info_dict[t_id]["EarlyStartDate"],
                                                                 "%Y-%m-%dT%H:%M:%S")).strftime("%Y-%m-%dT%H:%M:%S")
            split_late_finish_time = (datetime.datetime.strptime(task_info_dict[t_id]["LateFinishDate"],
                                                                 "%Y-%m-%dT%H:%M:%S")).strftime("%Y-%m-%dT%H:%M:%S")
            need_resource_value = 0
            for item2 in resource_tasks_dict[key][t_id]:
                if is_reuse:  # 可重用型资源取所有时间段最大的使用量
                    need_resource_value = item2["Value"] if item2["Value"] > need_resource_value else need_resource_value
                else:  # 消耗型资源取所有时间段的和
                    need_resource_value += item2["Value"]
            all_task_use_resource_dict.setdefault(t_id, []).append(
                [split_early_start_time, split_late_finish_time, need_resource_value])
        all_task_use_resource_dict = dict(
            sorted(all_task_use_resource_dict.items(), key=lambda x: (x[1][0][0], x[1][0][1])))  # 根据开始时间进行排序
        intervals, all_task_use_resource_dict, task_info_dict, resource_id, resource_name = r_time_limits_list, all_task_use_resource_dict, task_info_dict, key, \
            resources_info_dict[key][
                "Name"]
        # 用任务的区间划分，error记录需要删除的任务
        # 初始化结果列表，每个元素格式为(x, y, 原区间的下标, 划分来源列表)
        split_results = [[datetime.datetime.strptime(interval[0], "%Y-%m-%dT%H:%M:%S"),
                          datetime.datetime.strptime(interval[1], "%Y-%m-%dT%H:%M:%S"),
                          [intervals.index(interval), interval[2]], []] for interval in intervals]
        errors = list()
        for key1, value1 in all_task_use_resource_dict.items():
            no_intersection_count = 0
            error = dict()
            for v in value1:
                [g, h] = [datetime.datetime.strptime(_, "%Y-%m-%dT%H:%M:%S") for _ in v if v.index(_) < 2]
                new_results = []
                for x, y, original, sources in split_results:
                    # 判断区间是否相交并进行相应的划分
                    if y <= g or x >= h:
                        # 不相交的情况，保留原区间和其来源信息
                        new_results.append([x, y, original, sources])
                        no_intersection_count += 1
                    else:
                        if x < g:
                            # 划分前半部分，继承原始来源信息
                            new_results.append([x, g, original, sources.copy()])
                        # 划分中间部分，更新来源信息包含当前划分区间
                        new_sources = sources + [[key1, v[2], value1.index(v)]]
                        new_results.append([max(x, g), min(y, h), original, new_sources])
                        if y > h:
                            # 划分后半部分，继承原始来源信息
                            new_results.append([h, y, original, sources.copy()])
                split_results = new_results
            if no_intersection_count == len(value1) * len(split_results):  # 该任务的开始结束时间不在资源的所有可用时间段，该任务做不了
                error["Type"] = "R-1"
                error["Tasks"] = [{"Id": key1, "Name": task_info_dict[key1]['Name']}]
                error[
                    "Name"] = f"【{task_info_dict[key1]['Name']}】的最早开始和最迟结束时间不在资源【{resource_name}】的所有可用时间段"
                error["Resources"] = [{"Id": resource_id, "Name": resource_name}]
                # 资源的最早可以使用时间
                r_early_start_time = split_results[0][0].strftime("%Y-%m-%dT%H:%M:%S")
                # 任务的最早开始时间
                t_early_start_time = value1[0][0]
                if t_early_start_time > r_early_start_time:  # 任务开始时间靠后
                    # 将任务的最早开始时间设置为资源的最早开始时间 任务往前移d个单位
                    d = (datetime.datetime.strptime(t_early_start_time,
                                                    "%Y-%m-%dT%H:%M:%S") - datetime.datetime.strptime(
                        r_early_start_time, "%Y-%m-%dT%H:%M:%S")).total_seconds()
                    error["Suggestion"] = [
                        f"1.【{task_info_dict[key1]['Name']}】的【最早开始时间,最迟结束时间】整体前移{d}个时间单位"]
                else:  # 资源开始时间靠后
                    # 将任务的最早开始时间设置为资源的最早开始时间 任务往后移d个单位
                    d = (datetime.datetime.strptime(r_early_start_time,
                                                    "%Y-%m-%dT%H:%M:%S") - datetime.datetime.strptime(
                        t_early_start_time, "%Y-%m-%dT%H:%M:%S")).total_seconds()
                    error["Suggestion"] = [
                        f"1.【{task_info_dict[key1]['Name']}】的【最早开始时间,最迟结束时间】整体后移{d}个时间单位"]
                error["Value"] = d
                errors.append(error)
        error_all_list += errors
    return error_all_list


def main(data_set):
    """
    入口函数
    :param data_set: 数据集
    :return:
    """
    (
        lower,
        upper,
        n_var,
        task_info_dict,
        resources_info_dict,
        resource_tasks_dict,
        individual,
        task_time_lines_taskid_dict
    ) = preprocessing(data_set)

    error_all = list()
    error_time_constrained = time_constrained_check(data_set, task_info_dict)
    # error_resource_constrained = resource_constrained_check(resource_tasks_dict, resources_info_dict, task_info_dict)

    error_all += error_time_constrained
    # error_all += error_resource_constrained
    if len(error_all) == 0:
        error_code = 0
        information = "没有冲突"
    else:
        error_code = 1
        information = "有冲突"
    return {"SceneId": data_set["SceneId"], "SceneName": data_set["SceneName"], "ErrorCode": error_code,
            "Information": information, "Values": [], "Errors": error_all}


