"""
------------------------------- Reference --------------------------------
 Y. Wang, B. Wang, H. Li, G. G. Yen, Incorporating objective function
 information into the feasibility rule for constrained evolutionary
 optimization, IEEE Transactions on Cybernetics, 2015, 46(12): 1-15.
"""

import numpy as np

from .. import GeneticAlgorithm, utils, Population


class FROFI(GeneticAlgorithm):
    type = {
        "n_obj": "single",
        "encoding": "real",
        "special": {"large/none", "constrained"},
    }

    def __init__(
        self,
        pop_size,
        options,
        optimization_problem,
        control_cb,
        max_fe=10000,
        name="FROFI",
        show_bar=False,
        sim_req_cb=None,
        debug=False,
    ):
        super(FROFI, self).__init__(
            pop_size,
            options,
            optimization_problem,
            control_cb,
            max_fe=max_fe,
            name=name,
            show_bar=show_bar,
            sim_req_cb=sim_req_cb,
            debug=debug,
        )

    def run_algorithm(self):
        # Generate random Pop
        Pop = self.problem.init_pop()
        self.cal_obj(Pop)

        # Optimization
        while self.not_terminal(Pop):
            Offspring = self.Operator(Pop)
            Pop = self.EnvironmentalSelection(Pop, Offspring)
            Pop = self.Mutation(Pop, self.problem.lb, self.problem.ub)

    def Operator(self, Pop):
        # Parameter setting
        PopDec = Pop.decs
        N, D = PopDec.shape
        CR = np.array([0.1, 0.2, 1]).T
        CR = np.tile(np.random.choice(CR, size=(N, 1)), (1, D))
        F = np.array([0.6, 0.8, 1]).T
        F = np.tile(np.random.choice(F, size=(N, 1)), (1, D))
        # Parents
        P = np.argsort(np.random.random((N, N)), axis=1)
        P1 = PopDec[P[:, 0], :]
        P2 = PopDec[P[:, 1], :]
        P3 = PopDec[P[:, 2], :]
        B = np.argmin(Pop.objv)
        PB = np.tile(PopDec[B, :], (N, 1))
        # Offspring generation
        Rand = np.random.random((N, D))
        k1 = np.tile(np.random.random((N, 1)) < 0.5, (1, D))
        k2 = np.logical_and(~k1, np.random.random((N, D)) < CR)
        OffDec = PopDec
        OffDec[k1] = (
            PopDec[k1]
            + Rand[k1] * (P1[k1] - PopDec[k1])
            + F[k1] * (P2[k1] - P3[k1])
        )
        OffDec[k2] = (
            P1[k2] + Rand[k2] * (PB[k2] - P1[k2]) + F[k2] * (P2[k2] - P3[k2])
        )
        Offspring = Population(decs=OffDec)
        self.cal_obj(Offspring)
        return Offspring

    def EnvironmentalSelection(self, Pop, Offspring):
        # Pop update
        replace1 = utils.fitness_single(Pop) > utils.fitness_single(Offspring)
        replace2 = (
            np.logical_and(~replace1, (Pop.objv).flatten())
            > (Offspring.objv).flatten()
        )
        Pop[replace1] = Offspring[replace1]
        Archive = Offspring[replace2]
        # Replacement mechanism
        Nf = np.round(len(Pop) / np.round(np.maximum(5, len(Pop[1].decs) / 2)))
        rank = np.argsort(Pop.objv)[::-1]
        Pop = Pop[rank]
        for i in range(1, int(np.floor(len(Pop) / Nf))):
            if len(Archive) == 0:
                break
            else:
                current = np.array(
                    np.arange((i - 1) * Nf + 1, i * Nf + 1, dtype=np.int64)
                )
                worst = np.argmax(
                    np.sum(np.maximum(0, Pop[current].cv), axis=1)
                )
                best = np.argmin(np.sum(np.maximum(0, Archive.cv), axis=1))
                if Archive[best].objv < Pop[current[worst]].objv:
                    Pop[current[worst]] = Archive[best]
                    Archive[best] = np.delete(Archive, best, None)
        return Pop

    def Mutation(self, Pop, Lower, Upper):
        if ~np.any(np.all(Pop.cv <= 0, axis=1)):
            OffDec = Pop[np.random.randint(len(Pop))].decs
            k = np.random.randint(len(OffDec))
            OffDec[k] = np.random.uniform(Lower[k], Upper[k])
            Offspring = Population(decs=OffDec)
            self.cal_obj(Offspring)
            worst = np.argmax(np.sum(np.maximum(0, Pop.cv), axis=1))
            if Pop[worst].objv > Offspring.objv:
                Pop[worst] = Offspring
        return Pop
