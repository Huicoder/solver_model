from enum import Enum


class Conflict_T(Enum):
    EARLY_START_DATE = "最早开始时间"
    LATE_FINISH_DATE = "最迟结束时间"
    EXPECT_START_TIME = "期望开始时间"
    EXPECT_FINISH_TIME = "期望结束时间"
    REAL_START = "实际开始时间"
    REAL_FINISH = "实际结束时间"


if __name__ == "__main__":
    print(Conflict_T.EARLY_START_DATE.value == "最早开始时间")
