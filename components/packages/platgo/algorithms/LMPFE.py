import numpy as np
import scipy.io as sio
from ...common.commons import AlgoMode
from scipy.spatial.distance import cdist  # 参数一定要定义形状reshape(x,y) flatten()
from .. import GeneticAlgorithm, utils, operators


class LMPFE(GeneticAlgorithm):
    type = {
        "n_obj": {"multi", "many"},
        "encoding": {
            "real",
            "binary",
            "permutation",
            "label",
            "vrp",
            "two_permutation",
        },
        "special": "",
    }

    def __init__(
        self,
        pop_size,
        options,
        optimization_problem,
        control_cb,
        max_fe=10000,
        fPFE=0.1,
        K=5,
        name="LMPFE",
        show_bar=False,
        sim_req_cb=None,
        algo_mode=AlgoMode.ACADEMIC,
        ext_opt_prob_cb=None,
        debug=False,
    ):
        super(LMPFE, self).__init__(
            pop_size,
            options,
            optimization_problem,
            control_cb,
            max_fe=max_fe,
            name=name,
            show_bar=show_bar,
            sim_req_cb=sim_req_cb,
            ext_opt_prob_cb=ext_opt_prob_cb,
            debug=debug,
        )
        self.fPFE = fPFE
        self.K = K

    def run_algorithm(self):

        # Generate random population
        pop = self.problem.init_pop()

        self.cal_obj(pop)
        FrontNo, _ = utils.nd_sort(pop.objv, pop.pop_size)
        # Generate K subregions

        Center, R = self.adaptiveDivision(pop.objv, self.K)
        # Calculate the intersection point on each subregion
        # Initialze parameter P
        P = np.ones((self.K, self.problem.n_obj))

        # Calculate the fitness of each solution
        App, Dis = self.subFitness(pop.objv, P, Center, R)
        Dis = np.sort(Dis, 1)
        Crowd = (Dis[:, 0] + 0.1 * Dis[:, 1]).reshape(-1, 1)
        theta = 0.8
        preApp = np.mean(App)
        preCrowd = np.mean(Crowd)
        # Optimization
        while self.not_terminal(pop):
            # Mating
            MatingPool = utils.tournament_selection(
                2,
                pop.pop_size,
                FrontNo,
                (-theta * Crowd - (1 - theta) * App).flatten(),
            )

            Offspring = operators.OperatorGA(pop[MatingPool], self.problem)

            self.cal_obj(Offspring)
            # Generic front modeling
            if (
                not np.mod(
                    int(np.ceil(self._gen / pop.pop_size)),
                    int(
                        np.ceil(
                            self.fPFE * np.ceil(self._max_fe / pop.pop_size)
                        )  # noqa
                    ),
                )
                or self.fPFE == 0
            ):
                # Update subregions
                Center, R = self.adaptiveDivision(pop.objv, self.K)
                # % PF modeling
                P = self.subGFM(pop.objv, Center, R, FrontNo)
            temp_pop = pop + Offspring
            pop, FrontNo, App, Crowd = self.EnvironmentalSelection(
                temp_pop, P, theta, pop.pop_size, Center, R
            )
            # Update theta
            theta, preApp, preCrowd = self.UpdateTheta(
                preApp, preCrowd, App, Crowd
            )  # noqa
        return pop

    def adaptiveDivision(self, PopObj, K):
        [N, M] = np.shape(PopObj)

        if K == 1:
            Center = np.zeros((1, M))
            R = np.inf
            subNum = np.array([[1, N]])
        else:
            ## Detect the number of subregion
            # Calculate the distance between each solution
            fmin = np.min(PopObj, 0)
            fmax = np.max(PopObj, 0)

            PopObj = (PopObj - np.tile(fmin, (N, 1))) / np.tile(
                fmax - fmin, (N, 1)
            )  # noqa
            Distance = cdist(PopObj, PopObj)
            Distance[np.eye(N) > 0] = np.inf
            radius = np.max(np.min(Distance, 0))

            # Detect subregion(s)
            Transformation = np.zeros((N, 1))
            Remain = np.argwhere(Transformation == 0)[:, 0]
            RegionID = 1

            while not Remain.size == 0:
                seeds = np.argwhere(np.logical_not(Transformation))[:, 0][0]
                Transformation[seeds] = RegionID
                Remain = np.argwhere(Transformation == 0)[:, 0]
                while True:
                    # 对于高维数组取多行所列时，先对列取，再对行取
                    temp_col = Distance[:, Remain]
                    temp_row = temp_col[seeds, :]
                    neighbors = np.sum(
                        (temp_row <= radius).reshape(seeds.size, Remain.size),
                        0,
                    )
                    seeds = Remain[neighbors >= 1]
                    Transformation[seeds] = RegionID
                    Remain = np.argwhere(Transformation == 0)[:, 0]
                    if np.sum(neighbors) == 0:
                        break

                RegionID = RegionID + 1
            ## Region division
            # Count the number of subregions of the true PF

            TrueNum = np.unique(Transformation).size

            # Calculate the center point of each subregion
            Center = np.zeros((TrueNum, M))
            R = np.ones((TrueNum, 1))
            for i in range(TrueNum):
                current = Transformation == i + 1
                Center[i, :] = np.mean(PopObj[current.flatten(), :], 0)
                R[i] = np.max(
                    cdist(
                        PopObj[current.flatten(), :],
                        Center[i, :].reshape(1, M),
                    )
                )
            # Select K points
            temp_Transformation = Transformation.flatten()
            temp_arr = []
            # 计算每个元素出现的次数
            for k in np.unique(temp_Transformation):
                v = temp_Transformation[temp_Transformation == k].size
                temp_arr.append(v)
            # 将元素值与次数进行拼接
            subNum = np.stack(
                (np.unique(temp_Transformation), np.array(temp_arr)), axis=1
            )

            if TrueNum > K:
                # Merging small subregions
                while np.sum(subNum[:, 1] != np.inf) > K:
                    I = np.argmin(subNum[:, 1])
                    Center[I, :] = np.full((1, M), np.inf)
                    subNum[I, 1] = np.inf
                    R[I] = -np.inf
                    current = np.argwhere(Transformation == I + 1)[:, 0]
                    T = np.argmin(
                        cdist(PopObj[current.flatten(), :], Center), 1
                    )
                    Transformation[current] = (T + 1).reshape(-1, 1)

                    # Update reference point
                    Idx = np.argwhere(subNum[:, 1] != np.inf)[:, 0]
                    for k in range(Idx.size):
                        Center[Idx[k], :] = np.mean(
                            PopObj[Transformation.flatten() == Idx[k] + 1, :],
                            0,
                        )
                        R[Idx[k]] = np.max(
                            cdist(
                                PopObj[
                                    Transformation.flatten() == Idx[k] + 1, :
                                ],
                                Center[Idx[k], :].reshape(1, -1),
                            ),
                            0,
                        ) / np.sqrt(M - 1)

            elif TrueNum < K:
                # Splite large subregions
                while np.sum(subNum[:, 1] != -np.inf) < K:
                    I = np.argmax(subNum[:, 1])
                    Center[I, :] = np.full((1, M), -np.inf)
                    subNum[I, 1] = -np.inf
                    R[I] = -np.inf
                    current = np.argwhere(Transformation == I + 1)[:, 0]
                    T1 = np.argmax(
                        cdist(
                            PopObj[current, :],
                            PopObj[
                                current[np.random.randint(len(current))], :
                            ].reshape(1, -1),
                        ),
                        0,
                    )
                    T2 = np.argmax(
                        cdist(
                            PopObj[current, :],
                            PopObj[current[T1].flatten(), :],
                        ),
                        0,
                    )
                    T = (
                        np.argmin(
                            cdist(
                                PopObj[current, :],
                                PopObj[np.append(current[T1], current[T2]), :],
                            ),
                            1,
                        )
                        + 1
                    )
                    ExistNum = len(subNum[:, 0])
                    Transformation[current] = (T + ExistNum).reshape(-1, 1)

                    # Update reference point
                    Center = np.concatenate(
                        (
                            Center,
                            np.mean(
                                PopObj[
                                    (Transformation == ExistNum + 1).flatten(),
                                    :,
                                ],
                                0,
                            ).reshape(1, -1),
                            np.mean(
                                PopObj[
                                    (Transformation == ExistNum + 2).flatten(),
                                    :,
                                ],
                                0,
                            ).reshape(1, -1),
                        )
                    )

                    R = np.concatenate(
                        (
                            R,
                            0.5
                            * cdist(
                                Center[ExistNum, :].reshape(1, -1),
                                Center[ExistNum + 1, :].reshape(1, -1),
                            ),
                            0.5
                            * cdist(
                                Center[ExistNum, :].reshape(1, -1),
                                Center[ExistNum + 1, :].reshape(1, -1),
                            ),
                        )
                    )
                    subNum = np.vstack(
                        (subNum, np.array([ExistNum, sum(T == 1)]),)
                    )
                    subNum = np.vstack(
                        (subNum, np.array([ExistNum + 1, sum(T == 2)]),)
                    )

        # Select reference point
        select = np.abs(subNum[:, 1]) != np.inf
        Center = Center[select, :]
        R = R[select]

        return Center, R

    def subFitness(self, PopObj, P, Center, R):
        K = len(R)
        [N, M] = np.shape(PopObj)

        # Normalize the population
        fmin = np.min(PopObj, 0)
        fmax = np.max(PopObj, 0)
        Obj = (PopObj - np.tile(fmin, (N, 1))) / np.tile(fmax - fmin, (N, 1))

        # Calculate intersection point in each subregion
        if K == 1:
            InterPoint = self.interPoint(PopObj, P)
        else:
            InterPoint = np.ones((N, M))
            # Allocation
            transformation = self.Allocation(Obj, Center, R)
            for i in range(K):
                current = np.argwhere(transformation == i + 1)[:, 0]
                if len(current) != 0:
                    sInterPoint = self.interPoint(Obj[current, :], P[i, :])
                    InterPoint[current, :] = sInterPoint

        # Calculate the diversity and convergence of intersection points
        App = np.min(InterPoint - Obj, 1).reshape(-1, 1)

        # Calculate the diversity of each solution
        Dis = self.distMax(InterPoint)
        Dis[np.eye(len(Dis)) > 0] = np.inf

        return App, Dis

    def interPoint(self, PopObj, P):
        # Calcualte the approximation degree of each solution, and the distances
        # between the intersection points of the solutions
        N = np.shape(PopObj)[0]

        # Calculate the intersections by gradient descent
        P = np.tile(P, (N, 1))  # Powers
        r = np.ones((N, 1))  # Parameters to be optimized
        lamda = np.zeros((N, 1)) + 0.002  # Learning rates
        E = (np.sum((r * PopObj) ** P, 1) - 1).reshape(-1, 1)  # errors
        for i in range(1000):
            newr = r - lamda * E * np.sum(
                P * PopObj ** P * r ** (P - 1), 1
            ).reshape(  # noqa
                -1, 1
            )
            newE = (np.sum((newr * PopObj) ** P, 1) - 1).reshape(-1, 1)
            update = np.logical_and(
                newr > 0, np.sum(newE ** 2) < np.sum(E ** 2)
            )  # noqa
            r[update] = newr[update]
            E[update] = newE[update]
            lamda[update] = lamda[update] * 1.002
            lamda[np.logical_not(update)] = (
                lamda[np.logical_not(update)] / 1.002
            )  # noqa
        InterPoint = PopObj * r
        return InterPoint

    def Allocation(self, Obj, kPoint, R):
        K = len(R)
        N = np.shape(Obj)[0]

        # Allocation of each solution
        if K == 1:
            Transformation = np.ones((N, 1))
        else:
            Transformation = np.zeros((N, 1))
            for i in range(K):
                T = np.argwhere(Transformation != 0)[:, 0]
                current = cdist(kPoint[i, :].reshape(1, -1), Obj[T, :]) <= R[i]
                Transformation[T[current.flatten()]] = i + 1

            Remain = Transformation == 0

            if np.sum(Remain) != 0:
                transformation = (
                    np.argmin(cdist(Obj[Remain.flatten(), :], kPoint), 1) + 1
                )
                Transformation[Remain] = transformation
        return Transformation

    def distMax(self, X):

        if len(X) == 0:
            return None
        else:
            N = np.shape(X)[0]
            Dis = np.zeros((N, N))
            for i in range(N):
                for j in range(i + 1, N):
                    Dis[i, j] = np.max(np.abs(X[i, :] - X[j, :]))

            Dis = Dis + Dis.T
            return Dis

    def subGFM(self, PopObj, Center, R, FrontNo):
        K = np.shape(Center)[0]
        [N, M] = np.shape(PopObj)

        # Normalize the population
        fmin = np.min(PopObj, 0)
        fmax = np.max(PopObj, 0)
        Obj = (PopObj - np.tile(fmin, (N, 1))) / np.tile(fmax - fmin, (N, 1))
        # PF modeling
        if K == 1:
            P = self.GFM(Obj[FrontNo == 1, :])
        else:
            P = np.ones((K, M))
            # Allocation
            transformation = self.Allocation(Obj, Center, R)
            subFirstFront = np.full((N, 1), False).flatten()

            # Non-dominated sorting od each subregion
            for i in range(K):
                current = np.argwhere(transformation == i + 1)[:, 0]
                if len(current) > 0:
                    FNo, MFNo = utils.nd_sort(PopObj[current, :], len(current))
                    subFirstFront[
                        current[np.logical_or(FNo < MFNo, FNo == 1)]
                    ] = True
            FTransformation = transformation[subFirstFront]
            PopObj = PopObj[subFirstFront]
            RemainObj = Obj[subFirstFront]

            # GFM of each subregion
            if np.shape(PopObj)[0] > M:
                for i in range(K):
                    current = np.argwhere(FTransformation == i + 1)[:, 0]
                    if len(current) > 0:
                        if len(current) < M + 1:
                            sDis = np.argsort(
                                cdist(RemainObj, Center[i, :].reshape(1, -1)),
                                0,
                            )
                            current = sDis[0 : M + 1]
                        p = self.GFM(Obj[current.flatten(), :])
                        P[i, :] = p
        return P

    # Generic front modeling
    def GFM(self, X):
        [N, M] = np.shape(X)
        X = np.maximum(X, 1e-12)
        P = np.ones((1, M))
        lamda = 1
        E = np.sum(X ** np.tile(P, (N, 1)), 1) - 1
        MSE = np.mean(E ** 2, 0)
        for epoch in range(1000):
            # Calculate the Jacobian matrix
            J = X ** np.tile(P, (N, 1)) * np.log(X)
            # Update the value of each weight
            while True:
                # np.linalg.inv -> 求逆
                temp_1 = -np.linalg.inv(
                    (np.dot(J.T, J) + lamda * np.eye(np.size(J, 1)))
                )
                temp_2 = np.dot(temp_1, J.T)
                Delta = np.dot(temp_2, E)

                newP = P + Delta.T
                newE = np.sum(X ** np.tile(newP, (N, 1)), 1) - 1
                newMSE = np.mean(newE ** 2)
                if newMSE < MSE and np.all(newP > 1e-3):
                    P = newP
                    E = newE
                    MSE = newMSE
                    lamda = lamda / 1.08
                    break
                elif lamda > 1e8:
                    return P
                else:
                    lamda = lamda * 1.08
        return P

    def EnvironmentalSelection(self, pop, P, theta, N, Center, R):
        # Non-dominated sorting
        FrontNo, MaxFNo = utils.nd_sort(pop.objv, N)
        Next = np.argwhere(FrontNo <= MaxFNo)[:, 0]

        # Environmental selection
        App, Dis = self.subFitness(pop[Next].objv, P, Center, R)
        Choose = self.LastSelection(
            pop[Next].objv, FrontNo[Next], App, Dis, theta, N
        )

        # Population for next generation
        pop = pop[Next[Choose]]
        FrontNo = FrontNo[Next[Choose]]
        App = App[Choose]

        temp_col = Dis[:, Choose]
        tem_row = temp_col[Choose, :]
        Dis = np.sort(tem_row, 1)

        Crowd = (Dis[:, 0] + 0.1 * Dis[:, 1]).reshape(-1, 1)
        return pop, FrontNo, App, Crowd

    # Select part of the solutions in the last front
    def LastSelection(self, PopObj, FrontNo, App, Dis, theta, N):
        # Identify the extreme solutions
        NDS = np.argwhere(FrontNo == 1)[:, 0]
        Extreme = np.argmin(
            np.tile(
                np.sqrt(np.sum(PopObj[NDS, :] ** 2, 1)).reshape(-1, 1),
                (1, np.size(PopObj, 1)),
            )
            * np.sqrt(
                1
                - (
                    1
                    - cdist(
                        PopObj[NDS, :], np.eye(np.size(PopObj, 1)), "cosine"
                    )
                )
                ** 2
            ),
            0,
        )
        nonExtreme = np.logical_not(np.isin(range(len(FrontNo)), NDS[Extreme]))

        # Environmental selection
        Last = FrontNo == np.max(FrontNo)
        Choose = np.full((1, np.size(PopObj, 0)), True).flatten()

        # Non-dominated sort convergence and diversity
        while np.sum(Choose) > N:
            Remain = np.argwhere(np.logical_and(Choose, Last, nonExtreme))[
                :, 0
            ]
            # 同时取多行多列，先取行，再取列
            temp_col = Dis[:, Choose]
            temp_row = temp_col[Remain, :]
            dis = np.sort(temp_row, 1)
            dis = dis[:, 0] + 0.1 * dis[:, 1]
            fitness = theta * dis + (1 - theta) * App[Remain].flatten()
            worst = np.argmin(fitness, 0)
            Choose[Remain[worst]] = False
        return Choose

    def UpdateTheta(self, preApp, preCrowd, newApp, newCrowd):
        appNew = np.mean(newApp, 0)
        crowNew = np.mean(newCrowd, 0)

        # calculate the rates of change of converegnce and diversity
        rateApp = np.abs(appNew - preApp) / np.abs(preApp)
        rateCrowd = np.abs(crowNew - preCrowd) / np.abs(preCrowd)

        # update theta
        Z = rateApp - rateCrowd
        theta = 1 - np.exp(-np.exp(Z))
        preApp = appNew
        preCrowd = crowNew
        return theta, preApp, preCrowd
