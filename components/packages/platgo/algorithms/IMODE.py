import numpy as np
import scipy.stats as st
from .. import GeneticAlgorithm
from ..Population import Population


class IMODE(GeneticAlgorithm):
    type = {
        "n_obj": "single",
        "encoding": "real",
        "special": {"constrained/none", "large/none"},
    }

    def __init__(
        self,
        pop_size,
        options,
        optimization_problem,
        control_cb,
        max_fe=10000,
        name="IMODE",
        show_bar=False,
        sim_req_cb=None,
        debug=False,
    ):
        super(IMODE, self).__init__(
            pop_size,
            options,
            optimization_problem,
            control_cb,
            max_fe=max_fe,
            name=name,
            show_bar=show_bar,
            sim_req_cb=sim_req_cb,
            debug=debug,
        )
        self.minN = 4
        self.aRate = 2.6

    def run_algorithm(self):
        """
         main function for Different Evolution
         if population is None, generate a new population with N
        :param N: population size
        :param population: population to be optimized
        :return:
        """
        pop = self.problem.init_pop()
        self.cal_obj(pop)

        Archive = self.problem.init_pop(0)
        MCR = np.zeros((20 * self.problem.n_var, 1)) + 0.2
        MF = np.zeros((20 * self.problem.n_var, 1)) + 0.2
        k = 0
        MOP = np.ones((1, 3)) / 3

        while self.not_terminal(pop):
            # Reduce the population size

            N = np.int(
                (
                    np.ceil(
                        (self.minN - self.problem.pop_size)
                        * self._gen
                        / self._max_fe
                    )
                    + self.problem.pop_size
                )
            )
            rank = np.argsort(self.fitness_single(pop)[:, 0])

            pop = pop[rank[range(N)]]
            Archivemim = np.int(
                np.minimum(Archive.pop_size, np.ceil(self.aRate * N))
            )
            Archive = Archive[
                np.random.permutation(Archive.pop_size)[0:Archivemim]
            ]
            self.cal_obj(Archive)
            Xp1 = pop[
                np.ceil(np.random.rand(1, N) * max(1, 0.25 * N))[0].astype(
                    np.int64
                )
            ].decs.copy()

            Xp2 = pop[
                np.ceil(np.random.rand(1, N) * max(2, 0.5 * N))[0].astype(
                    np.int64
                )
            ].decs.copy()

            Xr1 = pop[np.random.randint(pop.pop_size, size=(1, N))].decs.copy()
            Xr3 = pop[np.random.randint(pop.pop_size, size=(1, N))].decs.copy()

            P = pop + Archive
            Xr2 = P[np.random.randint(pop.pop_size, size=(1, N))].decs.copy()
            CR = (
                np.random.randn(N, 1) * np.sqrt(0.1)
                + MCR[np.random.randint(pop.pop_size, size=(N, 1))[:, 0]]
            )
            CR = np.sort(CR)
            CR = np.tile(
                np.maximum(0, np.minimum(1, CR)), [1, self.problem.n_var]
            )
            F = np.minimum(
                1,
                st.t.rvs(1, size=(N, 1)) * np.sqrt(0.1)
                + MF[np.random.randint(pop.pop_size, size=(N, 1))[:, 0]],
            )

            while any(F <= 0):
                g = F <= 0
                F[g[:, 0]] = np.minimum(
                    1,
                    st.t.rvs(1, size=(sum(F <= 0)[0], 1)) * np.sqrt(0.1)
                    + MF[
                        np.random.randint(
                            pop.pop_size, size=(sum(F <= 0)[0], 1)
                        )[:, 0]
                    ],
                )

            F = np.tile(F, [1, self.problem.n_var])

            OP = np.zeros((N), dtype=np.int64)
            for i in range(N):
                rand = np.random.uniform(0, 1)
                for j in range(np.shape(MOP)[1]):
                    if j == 0:
                        if rand < np.cumsum(MOP)[j]:
                            OP[i] = j

                    else:
                        if (rand > np.cumsum(MOP)[j - 1]) and (
                            rand < np.cumsum(MOP)[j]
                        ):
                            OP[i] = j

            OP0 = np.zeros((np.sum(OP == 0)), dtype=np.int64)
            OP1 = np.zeros((np.sum(OP == 1)), dtype=np.int64)
            OP2 = np.zeros((np.sum(OP == 2)), dtype=np.int64)

            opindex0 = 0
            opindex1 = 0
            opindex2 = 0

            for i in range(pop.pop_size):
                if OP[i] == 0:
                    OP0[opindex0] = i
                    opindex0 = opindex0 + 1
                elif OP[i] == 1:
                    OP1[opindex1] = i
                    opindex1 = opindex1 + 1
                elif OP[i] == 2:
                    OP2[opindex2] = i
                    opindex2 = opindex2 + 1

            PopDec = pop.decs.copy()
            OffDec = PopDec
            OffDec[OP0, :] = PopDec[OP0, :] + F[OP0, :] * (
                Xp1[OP0, :] - PopDec[OP0, :] + Xr1[OP0, :] - Xr2[OP0, :]
            )
            OffDec[OP1, :] = PopDec[OP1, :] + F[OP1, :] * (
                Xp1[OP1, :] - PopDec[OP1, :] + Xr1[OP1, :] - Xr3[OP1, :]
            )
            OffDec[OP2, :] = F[OP2, :] * (
                Xr1[OP2, :] + Xp2[OP2, :] - Xr3[OP2, :]
            )

            if np.random.rand() < 0:
                Site = np.random.rand(np.shape(CR)[0], np.shape(CR)[1]) < CR
                OffDec[Site] = PopDec[Site]
            else:
                p1 = np.random.randint(self.problem.n_var, size=(N, 1))
                p2 = np.zeros((N, 1))
                for i in range(N):
                    rand = np.random.rand(1, self.problem.n_var)[0]
                    rand = np.append(rand, 2)
                    for j in range(np.shape(rand)[0]):
                        if rand[j] > CR[i, 0]:
                            p2[i] = j
                            break

                for i in range(N):
                    # Site =[1:p1[i,0]-1,p1[i,0]+p2[i,0]:self.problem.n_var]
                    Site = np.append(
                        np.arange(0, p1[i, 0] - 1),
                        np.arange(
                            np.int(p1[i, 0] + p2[i, 0]), self.problem.n_var
                        ),
                    )
                    OffDec[i, Site] = PopDec[i, Site]

            Offspring = Population(decs=OffDec)
            self.cal_obj(Offspring)

            delta = self.fitness_single(pop) - self.fitness_single(Offspring)
            delta = delta[:, 0]
            replace = delta > 0
            Archive = Archive + pop[replace]
            Archivemim = np.minimum(Archive.pop_size, np.ceil(self.aRate * N))
            Archivemim = np.int(Archivemim)
            Archive = Archive[
                np.random.permutation(Archive.pop_size)[0:Archivemim]
            ]
            self.cal_obj(Archive)
            pop[replace] = Offspring[replace]

            if any(replace):
                w = delta[replace] / sum(delta[replace])
                w = np.reshape(w, (-1, 1))
                w1 = np.reshape(CR[replace, 1], (-1, 1))
                MCR[k, 0] = (np.dot(w.T, w1)[0, 0]) ** 2 / (
                    np.dot(w.T, w1)[0, 0]
                )
                w2 = np.reshape(F[replace, 1], (-1, 1))

                MF[k, 0] = (np.dot(w.T, w2 ** 2)[0, 0]) / (
                    np.dot(w.T, w2)[0, 0]
                )
                k = np.mod(k, np.shape(MCR)[0]) + 1
            else:
                MCR[k, 0] = 0.5
                MF[k, 0] = 0.5

            delta = np.maximum(
                0, delta / np.abs(self.fitness_single(pop))[:, 0]
            )
            if OP0.shape[0] == 0 or OP1.shape[0] == 0 or OP2.shape[0] == 0:
                MOP = np.ones((1, 3)) / 3
            else:
                MOP[0, 0] = np.mean(delta[OP0])
                MOP[0, 1] = np.mean(delta[OP1])
                MOP[0, 2] = np.mean(delta[OP2])
                MOP = np.maximum(0.1, np.minimum(0.9, MOP / np.sum(MOP)))

        return pop

    def fitness_single(self, pop: Population):
        pop_con = np.sum(np.where(pop.cv < 0, 0, pop.cv), axis=1)
        feasible = pop_con <= 0
        fitness = feasible * pop.objv + ~feasible * np.all(pop_con + 1e10)
        return fitness
