# python基础镜像
FROM python:3.10
LABEL authors="谢国辉"
RUN pip install --upgrade pip
# 创建app文件夹
RUN mkdir -p /app
# 设置工作目录
WORKDIR /app
# 进入app目录
RUN cd /app
# 复制项目所有文件到工作目录
ADD . /app
# 更换pip镜像为清华源
RUN pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple
# 暴露应用程序端口号
EXPOSE 1433

# 运行应用程序 使用gunicorn服务器启动flask应用
CMD ["gunicorn","-c","gunicorn_config.py","app:app"]
