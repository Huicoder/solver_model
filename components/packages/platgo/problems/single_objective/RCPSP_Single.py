import datetime
import copy
import numpy as np
from ...Problem import Problem
from ....platgo import Population


class RCPSP_Single(Problem):
    """
    type = {"n_obj": "single", "encoding": "real"}
    """

    def __init__(self, in_optimization_problem, debug=True) -> None:
        optimization_problem = {
            "mode": 0,
            "encoding": "real",
            "n_obj": 1,
        }
        optimization_problem.update(in_optimization_problem)
        self.task_info_dict = optimization_problem["task_info_dict"]
        self.resources_info_dict = optimization_problem["resources_info_dict"]
        self.resource_tasks_dict = optimization_problem["resource_tasks_dict"]
        self.individual = optimization_problem["individual"]
        super(RCPSP_Single, self).__init__(
            optimization_problem, debug=debug
        )

    def init_pop(self, N: int = None):
        if N is None:
            N = self.pop_size
        # 将lb矩阵向0维坐标方向重复n次
        lb = np.tile(self.lb, (N, 1))
        # 将ub矩阵向0维坐标方向重复n次
        ub = np.tile(self.ub, (N, 1))
        decs = np.random.randint(lb, ub)  # 不包含右边界
        decs[0] = self.individual  # 初始解
        return Population(decs=decs)

    def fix_decs(self, pop: Population):
        # 对边界进行修复
        for i in range(len(pop)):
            for j in range(pop.decs.shape[1]):
                if self.lb[j] <= pop.decs[i][j] < self.ub[j]:
                    pop.decs[i][j] = int(pop.decs[i][j])
                else:
                    pop.decs[i][j] = np.random.randint(self.lb[j], self.ub[j])
        return pop

    def compute(self, pop) -> None:
        objv = np.zeros((pop.decs.shape[0], self.n_obj))
        finalresult = np.empty((pop.decs.shape[0], 1), dtype=np.object_)
        for i, x in enumerate(pop.decs):
            objv[i], finalresult[i] = self.main(x)
        pop.objv = objv
        pop.finalresult = finalresult
        pop.cv = np.zeros((pop.pop_size, self.n_constr))

    def merge_dicts(self,dict1, dict2):
        return {k: dict1.get(k, []) + dict2.get(k, []) for k in set(dict1) | set(dict2)}

    def main(self, x):
        task_time_done_dict, time_error_info_dict = self.time_constrained_check(x)
        task_resources_done_dict, resources_error_info_dict = self.resource_constrained_check(task_time_done_dict)
        final_result_dict = dict()
        if len(time_error_info_dict) and len(resources_error_info_dict) != 0:
            final_result_dict["Error"] = self.merge_dicts(time_error_info_dict,resources_error_info_dict)
        else:
            if len(resources_error_info_dict) != 0:
                final_result_dict["Error"] = resources_error_info_dict
            elif len(time_error_info_dict) != 0:
                final_result_dict["Error"] = time_error_info_dict
            else:
                final_result_dict["Error"] = None
        # final_result_dict["Error"] = time_error_info_dict.update(resources_error_info_dict) if len(
        #     resources_error_info_dict) != 0 else time_error_info_dict
        final_result_dict["Real"] = task_resources_done_dict
        return self.n_var - len(task_resources_done_dict), f'{final_result_dict}'

    def getTime(self, task_id, time_type, x, task_id_list):
        real_start_time = datetime.datetime.strptime(self.task_info_dict[task_id]["EarlyStartDate"],
                                                     "%Y-%m-%dT%H:%M:%S") + datetime.timedelta(
            seconds=int(x[task_id_list.index(task_id)]))
        real_finish_time = real_start_time + datetime.timedelta(
            seconds=self.task_info_dict[task_id]["NeedTimes"])
        if time_type == 0:  # 实际开始时间
            time = real_start_time.strftime("%Y-%m-%dT%H:%M:%S")
        elif time_type == 1:  # 实际结束时间
            time = real_finish_time.strftime("%Y-%m-%dT%H:%M:%S")
        elif time_type == 2:  # 最早开始时间
            time = self.task_info_dict[task_id]["EarlyStartDate"]
        elif time_type == 3:  # 最迟结束时间
            time = self.task_info_dict[task_id]["LateFinishDate"]
        elif time_type == 4:  # 期望开始时间
            time = self.task_info_dict[task_id]["ExpectStartTime"]
        else:  # 期望结束时间
            time = self.task_info_dict[task_id]["ExpectFinishTime"]
        return time

    def time_constrained_check(self, x):
        """
        任务时间约束检查
        :return: 满足时间约束的任务
        """
        data_set = self.data[0]
        task_id_list = list(self.task_info_dict.keys())
        task_time_done_dict = dict()  # 记录满足时间约束的任务
        error_info_dict = dict()  # 记录哪些任务违反时间约束及其信息
        # Type-> 0：实际开始时间 RealStart 1：实际结束时间 RealFinish 2：最早开始时间 EarlyStartDate 3：最迟结束时间 LateFinishDate
        # Compare-> 0：等于 1：大于 2：大于等于 3：小于 4：小于等于
        for task_id in task_id_list:  # 对每个任务进行遍历
            flag = True  # 如何违反了对该任务的时间约束置为false
            for i in range(len(data_set["TaskTimeLines"])):
                task_limits = data_set["TaskTimeLines"][i]["TaskLimits"]
                for item in task_limits:  # 对约束列表进行遍历
                    before_task_id = item["BeforeTaskId"]
                    if task_id == before_task_id:  # 找到对每个任务的约束
                        before_time_type = item["BeforeTimeType"]
                        after_task_id = item["AfterTaskId"]
                        after_time_type = item["AfterTimeType"]
                        compare = item["Compare"]
                        value = item["Value"]
                        before_time = self.getTime(before_task_id, before_time_type, x, task_id_list)
                        after_time = self.getTime(after_task_id, after_time_type, x, task_id_list)
                        real_value = (datetime.datetime.strptime(after_time,
                                                                 "%Y-%m-%dT%H:%M:%S") - datetime.datetime.strptime(
                            before_time, "%Y-%m-%dT%H:%M:%S")).total_seconds()
                        if compare == 0:
                            operator_str = "!="
                        elif compare == 1:
                            operator_str = "<="
                        elif compare == 2:
                            operator_str = "<"
                        elif compare == 3:
                            operator_str = ">="
                        else:
                            operator_str = ">"
                        if eval(f'{real_value} {operator_str} {value}'):
                            flag = False
                            # error_info_dict[before_task_id] = [1, after_task_id, real_value, before_time_type, after_time_type]
                            error_info_dict.setdefault(before_task_id, []).append(
                                [1, after_task_id, real_value, before_time_type, after_time_type])
            if flag:  # 与该任务的相关的所有时间约束全部满足
                real_start_time = datetime.datetime.strptime(self.task_info_dict[task_id]["EarlyStartDate"],
                                                             "%Y-%m-%dT%H:%M:%S") + datetime.timedelta(
                    seconds=int(x[task_id_list.index(task_id)]))
                real_finish_time = real_start_time + datetime.timedelta(
                    seconds=self.task_info_dict[task_id]["NeedTimes"])
                task_time_done_dict[task_id] = [real_start_time.strftime("%Y-%m-%dT%H:%M:%S"),
                                                real_finish_time.strftime("%Y-%m-%dT%H:%M:%S")]
        return task_time_done_dict, error_info_dict

    def split_intervals(self, intervals, all_task_use_resource_dict: dict):
        """
        划分区间并记录划分后的区间对应的原区间及所有划分来源。
        :param intervals: 原始区间列表，每个区间表示为(x, y)。
        :param all_task_use_resource_dict: 用于划分的区间信息
        :return: 划分后的区间列表，包括原区间和所有划分来源。
        """
        # 初始化结果列表，每个元素格式为(x, y, 原区间的下标, 划分来源列表)
        split_results = [[datetime.datetime.strptime(interval[0], "%Y-%m-%dT%H:%M:%S"),
                          datetime.datetime.strptime(interval[1], "%Y-%m-%dT%H:%M:%S"),
                          [intervals.index(interval), interval[2]], []] for interval in intervals]
        del_task_list = list()
        for key, value in all_task_use_resource_dict.items():
            no_intersection_count = 0
            for v in value:
                [g, h] = [datetime.datetime.strptime(_, "%Y-%m-%dT%H:%M:%S") for _ in v if v.index(_) < 2]
                new_results = []
                for x, y, original, sources in split_results:
                    # 判断区间是否相交并进行相应的划分
                    if y <= g or x >= h:
                        # 不相交的情况，保留原区间和其来源信息
                        new_results.append([x, y, original, sources])
                        no_intersection_count += 1
                    else:
                        if x < g:
                            # 划分前半部分，继承原始来源信息
                            new_results.append([x, g, original, sources.copy()])
                        # 划分中间部分，更新来源信息包含当前划分区间
                        new_sources = sources + [[key, v[2], value.index(v)]]
                        new_results.append([max(x, g), min(y, h), original, new_sources])
                        if y > h:
                            # 划分后半部分，继承原始来源信息
                            new_results.append([h, y, original, sources.copy()])
                split_results = new_results
            if no_intersection_count == len(value) * len(split_results):  # 该任务的开始结束时间不在资源的所有可用时间段，该任务做不了
                del_task_list.append(key)
        split_results = [[s_p[0].strftime("%Y-%m-%dT%H:%M:%S"), s_p[1].strftime("%Y-%m-%dT%H:%M:%S"), s_p[2], s_p[3]]
                         for s_p in split_results]
        return split_results, del_task_list


    def split_intervals2(self, intervals, all_task_use_resource_dict: dict):
        """
        划分区间并记录划分后的区间对应的原区间及所有划分来源。
        :param intervals: 原始区间列表，每个区间表示为(x, y)。
        :param all_task_use_resource_dict: 用于划分的区间信息
        :return: 划分后的区间列表，包括原区间和所有划分来源。
        """
        split_results = []
        # 处理所有的原始区间和分割区间
        all_intervals = intervals + sum(list(all_task_use_resource_dict.values()), [])
        all_points = set()
        for all_ in all_intervals:
            all_points.update([all_[0], all_[1]])
        sorted_points = sorted(all_points)

        # 根据所有关键点生成全覆盖区间
        full_cover_intervals = []
        for i in range(len(sorted_points) - 1):
            full_cover_intervals.append((sorted_points[i], sorted_points[i + 1]))

        # 遍历每个全覆盖区间，判断它们与原始区间和分割区间的关系
        for start, end in full_cover_intervals:
            # 判断当前全覆盖区间是否属于任一原始区间
            original_cover = [[intervals.index(ori), ori[2]] for ori in intervals if ori[0] <= start and ori[1] >= end]
            if original_cover:
                original = original_cover[0]  # 取第一个完全覆盖的原始区间
            else:
                original = 'external'

            # 判断当前全覆盖区间是否被分割区间覆盖，并记录所有覆盖它的分割区间
            cover_sources = list()
            for key, value in all_task_use_resource_dict.items():
                for v in value:  # 对任务的各个时间段进行循环
                    if v[0] <= start and v[1] >= end:
                        cover_sources.append([key, v[2], value.index(v)])
            # 添加到结果中
            split_results.append([start, end, original, cover_sources])

        return split_results

    def resource_constrained_check(self, task_time_done_dict: dict):
        error_info_dict = dict()  # 存储不满足资源约束的信息
        del_task_all_list = list()  # 存储判断每种资源时不能做的任务集合
        # 对每种资源进行遍历
        for key, value in self.resource_tasks_dict.items():
            # 找出该资源的所有的可用时间段
            r_time_limits_list = list()
            resources_info_dict = copy.deepcopy(self.resources_info_dict)
            # 共享资源（true）：多个任务可同时使用；独占资源（false）：只能有一个任务同时使用
            is_share = resources_info_dict[key]["IsShare"]
            # 重用资源（true）：可以重复使用；消耗资源（false）：使用1次后就没有了
            is_reuse = resources_info_dict[key]["IsReuse"]
            for item in resources_info_dict[key]["TimeLimits"]:
                r_time_limits_list.append([item["BeginTime"], item["EndTime"], item["MaxValue"]])
            # 找出使用该资源的所有的任务
            all_task_use_resource_dict = dict()
            for t_id in list(self.resource_tasks_dict[key].keys()):
                if t_id in task_time_done_dict:
                    for item2 in self.resource_tasks_dict[key][t_id]:
                        split_real_start_time = (datetime.datetime.strptime(task_time_done_dict[t_id][0],
                                                                            "%Y-%m-%dT%H:%M:%S") + datetime.timedelta(
                            seconds=item2["BeginTime"])).strftime("%Y-%m-%dT%H:%M:%S")
                        split_real_finish_time = (datetime.datetime.strptime(task_time_done_dict[t_id][0],
                                                                             "%Y-%m-%dT%H:%M:%S") + datetime.timedelta(
                            seconds=item2["EndTime"])).strftime("%Y-%m-%dT%H:%M:%S")
                        need_resource_value = item2["Value"]

                        all_task_use_resource_dict.setdefault(t_id, []).append(
                            [split_real_start_time, split_real_finish_time, need_resource_value])
            all_task_use_resource_dict = dict(
                sorted(all_task_use_resource_dict.items(), key=lambda x: (x[1][0][0], x[1][0][1])))  # 根据开始时间进行排序
            # 用任务的区间划分，del_task_list记录需要删除的任务
            split_intervals = self.split_intervals2(r_time_limits_list, all_task_use_resource_dict)
            del_task_list = list()
            error_info_resource_dict = dict()
            for sp_inter in split_intervals:
                if sp_inter[2] == 'external':
                    for r_t in r_time_limits_list:
                        for s_ in sp_inter[3]:
                            if sp_inter[0] < r_t[0]:  # 早于资源的开始时间
                                error_info_resource_dict.setdefault(s_[0], []).append("R-1-F")
                            elif sp_inter[1] > r_t[0]:  # 晚于资源的结束时间
                                error_info_resource_dict.setdefault(s_[0], []).append("R-2-F")
                            if s_[0] not in del_task_list:
                                del_task_list.append(s_[0])
            if len(del_task_list) != 0:
                for d_t in del_task_list:
                    # error_info_dict[d_t] = [3, key, "不在资源的可用时段内"]
                    error_info_dict.setdefault(d_t, []).append([3, key, "不在资源的可用时段内"])
            consume_resource_list = list()  # 记录已经分配消耗型资源的任务
            for sp_inter in split_intervals:  # 对划分后的区间进行遍历
                if sp_inter[2] != "external":  # 该资源的最大可用数量为零
                    MaxValue = sp_inter[2][1]  # 记录最大可用数量
                    if len(sp_inter[3]) > 0:  # 有任务在这个时段使用该资源
                        for s_p in sp_inter[3]:  # 对使用该资源的任务集合进行遍历
                            index = 0  # 判断独占型的计数下标，大于1时的当前任务不能做
                            if is_share:  # 共享型
                                if is_reuse:  # 共享可重用型
                                    if s_p[0] not in del_task_list:  # 当前任务不在不能做的列表中
                                        MaxValue -= s_p[1]  # 减去当前任务所用的资源数
                                        if MaxValue < 0:  # 任务的使用量超过资源总数
                                            # error_info_dict[s_p[0]] = [3, key, -MaxValue]
                                            error_info_dict.setdefault(s_p[0], []).append([3, key, -MaxValue])
                                            MaxValue += s_p[1]  # 对资源进行还原
                                            del_task_list.append(s_p[0])  # 该任务不能做
                                else:  # 共享消耗型
                                    if s_p[0] not in del_task_list:  # 当前任务不在不能做的列表中
                                        if s_p not in consume_resource_list:  # 当前任务不在已分配消耗型资源的任务的列表中
                                            MaxValue -= s_p[1]  # 减去当前任务所用的资源数
                                            if MaxValue < 0:  # 任务的使用量超过资源总数
                                                # error_info_dict[s_p[0]] = [3, key, -MaxValue]
                                                error_info_dict.setdefault(s_p[0], []).append([3, key, -MaxValue])
                                                MaxValue += s_p[1]  # 消耗型资源进行还原
                                                del_task_list.append(s_p[0])  # 该任务不能做
                                            else:
                                                consume_resource_list.append(s_p)  # 添加到已分配消耗型资源的列表
                                        # else:  # 当前任务已经分配过消耗型资源,从资源的总数开始重新分配
                                        #     MaxValue -= s_p[1]
                            else:  # 独占型
                                if is_reuse:  # 独占可重用型
                                    if s_p[0] not in del_task_list:
                                        if index == 0:  # 独占型资源只允许一个资源使用
                                            MaxValue -= s_p[1]  # 减去当前任务所用的资源数
                                            if MaxValue < 0:  # 任务的使用量超过资源总数
                                                # error_info_dict[s_p[0]] = [3, key, -MaxValue]
                                                error_info_dict.setdefault(s_p[0], []).append([3, key, -MaxValue])
                                                MaxValue += s_p[1]  # 对资源进行还原
                                                del_task_list.append(s_p[0])  # 该任务不能做
                                            else:  # 资源满足可以跳到下一个任务
                                                index += 1
                                        else:  # 还有其余的任务使用该独占资源
                                            # error_info_dict[s_p[0]] = [3, key, "独占资源不能共享"]
                                            error_info_dict.setdefault(s_p[0], []).append([3, key, "独占资源不能共享"])
                                            del_task_list.append(s_p[0])  # 其余任务不可做
                                else:  # 独占消耗型
                                    if s_p[0] not in del_task_list:  # 当前任务不在不能做的列表中
                                        if s_p not in consume_resource_list:  # 当前任务不在已分配消耗型资源的任务的列表中
                                            if index == 0:  # 独占型资源只允许一个资源使用
                                                MaxValue -= s_p[1]  # 减去当前任务所用的资源数
                                                if MaxValue < 0:  # 任务的使用量超过资源总数
                                                    # error_info_dict[s_p[0]] = [3, key, -MaxValue]
                                                    error_info_dict.setdefault(s_p[0], []).append([3, key, -MaxValue])
                                                    MaxValue += s_p[1]  # 进行还原
                                                    del_task_list.append(s_p[0])  # 该任务不能做
                                                else:  # 资源满足可以跳到下一个任务
                                                    consume_resource_list.append(s_p)  # 添加到已分配消耗型资源的列表
                                                    index += 1
                                            else:  # 还有其余的任务使用该独占资源
                                                # error_info_dict[s_p[0]] = [3, key, "独占资源不能共享"]
                                                error_info_dict.setdefault(s_p[0], []).append([3, key, "独占资源不能共享"])
                                                del_task_list.append(s_p[0])  # 其余任务不可做
                                        else:  # 当前任务已经分配过消耗型资源,从资源的总数开始重新分配
                                            # MaxValue -= s_p[1]
                                            index += 1
                    if is_reuse is False:  # 消耗型资源需要更新最大可用量
                        for element in split_intervals:
                            if element[2] != "external":  # 考虑到资源分段不连续 如果这个时间段资源可用量不为零
                                element[2][1] = MaxValue
            del_task_all_list.append(del_task_list)
        diff = list(set(task_time_done_dict.keys()) - set(sum(del_task_all_list, [])))
        for t_d in list(set(sum(del_task_all_list, []))):
            del task_time_done_dict[t_d]
        return task_time_done_dict, error_info_dict
