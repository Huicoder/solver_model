import copy
from queue import Queue  # noqa
import numpy as np
import ast


def get_description(key):
    if key == 0:
        description = "实际开始时间"
    elif key == 1:
        description = "实际结束时间"
    elif key == 2:
        description = "最早开始时间"
    elif key == 3:
        description = "最迟结束时间"
    elif key == 4:
        description = "期望开始时间"
    else:
        description = "期望结束时间"
    return description


def postprocessing(data_set, algo_result, task_info_dict, resources_info_dict, task_time_lines_taskid_dict):
    """
    后处理
    :param resources_info_dict:
    :param task_time_lines_taskid_dict:
    :param data_set:
    :param algo_result:
    :param task_info_dict:
    :return:
    """
    Values_list = list()
    Errors_list = list()
    n_var = algo_result["n_var"]
    n_obj = algo_result["n_obj"]
    algo_res = algo_result["data"]
    del algo_res[0]
    algo_res = np.array(algo_res)
    objv_min_index = np.argmin(
        np.array(algo_res[:, n_var + n_obj - 1], dtype=np.float32)
    )
    objv_max = n_var - float(algo_res[objv_min_index][-2])
    error_code = 0
    if objv_max == n_var:
        information = "求解成功"
    else:
        information = "求解失败"
        error_code = 1
    result = algo_res[objv_min_index][-1]  # 取出最后的输出结果
    for key, value in ast.literal_eval(result).items():
        if key == "Real" and value is not None:  # 添加每个任务的实际开始和结束时间
            for i in range(len(data_set["TaskTimeLines"])):
                if len(data_set["TaskTimeLines"][i]["Tasks"]) != 0:
                    Values = dict()
                    Values["TimeLineId"] = data_set["TaskTimeLines"][i]["TimeLineId"]
                    Values["Name"] = data_set["TaskTimeLines"][i]["Name"]
                    Values["Tasks"] = []
                    for key2, value2 in value.items():
                        if key2 in task_time_lines_taskid_dict[Values["TimeLineId"]]:
                            resource_usages = copy.deepcopy(task_info_dict[key2]["ResourceUsages"])
                            del task_info_dict[key2]["ResourceUsages"]
                            task = {"TaskId": key2}
                            task.update(task_info_dict[key2])
                            task.update({"RealStart": value2[0]})
                            task.update({"RealFinish": value2[1]})
                            task.update({"ResourceUsages": resource_usages})
                            Values["Tasks"].append(task)
                    Values_list.append(Values)
        if key == "Error" and value is not None:
            for key2, value2_ in value.items():
                for value2 in value2_:
                    Errors = dict()
                    Errors["Type"] = value2[0]
                    if Errors["Type"] == 1:
                        des1 = get_description(value2[3])
                        des2 = get_description(value2[4])
                        Errors["Tasks"] = [{"Id": key2, "Name": task_info_dict[key2]["Name"]},
                                           {"Id": value2[1], "Name": task_info_dict[value2[1]]["Name"]}]
                        Errors["Resources"] = []
                        Errors[
                            "Name"] = f'前置【{task_info_dict[key2]["Name"]}】的【{des1}】与后置【{task_info_dict[value2[1]]["Name"]}】的【{des2}】冲突值为【{value2[2]}】'
                        Errors["Value"] = value2[2]
                        Errors["Suggestion"] = []
                    else:
                        Errors["Tasks"] = [{"Id": key2, "Name": task_info_dict[key2]["Name"]}]
                        Errors["Resources"] = [{"Id": value2[1], "Name": resources_info_dict[value2[1]]["Name"]}]
                        if type(value2[2]) == str:
                            Errors[
                                "Name"] = f'【{task_info_dict[key2]["Name"]}】使用【{resources_info_dict[value2[1]]["Name"]}】的错误信息为:【{value2[2]}】'
                            Errors["Value"] = ""
                        else:
                            Errors[
                                "Name"] = f'【{task_info_dict[key2]["Name"]}】中资源【{resources_info_dict[value2[1]]["Name"]}】最大需求量高于最大可用量，最大可用量建议增加【{value2[2]}】'
                            Errors["Value"] = value2[2]
                        Errors["Suggestion"] = []
                    Errors_list.append(Errors)
    return objv_max, {"SceneId": data_set["SceneId"], "SceneName": data_set["SceneName"], "ErrorCode": error_code,
                      "Information": information, "Values": Values_list, "Errors": Errors_list}


def postprocessing_fixed(data_set, result, task_info_dict, resources_info_dict, task_time_lines_taskid_dict):
    """
    后处理
    :param resources_info_dict:
    :param task_time_lines_taskid_dict:
    :param data_set:
    :param result:
    :param task_info_dict:
    :return:
    """
    Values_list = list()
    Errors_list = list()

    error_code = 0
    if len(result["Error"]) == 0:
        information = "求解成功"
    else:
        information = "求解失败"
        error_code = 1
    for key, value in result.items():
        if key == "Real" and value is not None:  # 添加每个任务的实际开始和结束时间
            for i in range(len(data_set["TaskTimeLines"])):
                if len(data_set["TaskTimeLines"][i]["Tasks"]) != 0:
                    Values = dict()
                    Values["TimeLineId"] = data_set["TaskTimeLines"][i]["TimeLineId"]
                    Values["Name"] = data_set["TaskTimeLines"][i]["Name"]
                    Values["Tasks"] = []
                    for key2, value2 in value.items():
                        if key2 in task_time_lines_taskid_dict[Values["TimeLineId"]]:
                            resource_usages = copy.deepcopy(task_info_dict[key2]["ResourceUsages"])
                            del task_info_dict[key2]["ResourceUsages"]
                            task = {"TaskId": key2}
                            task.update(task_info_dict[key2])
                            task.update({"RealStart": value2[0]})
                            task.update({"RealFinish": value2[1]})
                            task.update({"ResourceUsages": resource_usages})
                            Values["Tasks"].append(task)
                    Values_list.append(Values)
        if key == "Error" and value is not None:
            for key2, value2_ in value.items():
                for value2 in value2_:
                    Errors = dict()
                    if value2[0] == 'R-1-F':
                        Errors["Tasks"] = [{"Id": key2, "Name": task_info_dict[key2]["Name"]}]
                        Errors["Resources"] = [{"Id": value2[1], "Name": resources_info_dict[value2[1]]["Name"]}]
                        Errors[
                            "Name"] = f'【{task_info_dict[key2]["Name"]}】的实际开始时间早于【{resources_info_dict[value2[1]]["Name"]}】的开始时间'
                        Errors["Value"] = ""
                    if value2[0] == 'R-2-F':
                        Errors["Tasks"] = [{"Id": key2, "Name": task_info_dict[key2]["Name"]}]
                        Errors["Resources"] = [{"Id": value2[1], "Name": resources_info_dict[value2[1]]["Name"]}]
                        Errors[
                            "Name"] = f'【{task_info_dict[key2]["Name"]}】的实际结束时间晚于【{resources_info_dict[value2[1]]["Name"]}】的结束时间'
                        Errors["Value"] = ""
                    if value2[0] == 'R-3-F':
                        Errors["Tasks"] = [{"Id": key2, "Name": task_info_dict[key2]["Name"]}]
                        Errors["Resources"] = [{"Id": value2[1], "Name": resources_info_dict[value2[1]]["Name"]}]
                        Errors[
                            "Name"] = f'【{task_info_dict[key2]["Name"]}】的实际开始时间早于【{resources_info_dict[value2[1]]["Name"]}】的开始时间，实际结束时间晚于{resources_info_dict[value2[1]]["Name"]}的结束时间'
                        Errors["Value"] = ""
                    if value2[0] == 'R-4-F':
                        Errors["Tasks"] = []
                        Errors["Resources"] = [{"Id": key2, "Name": resources_info_dict[key2]["Name"]}]
                        Errors[
                            "Name"] = f'【{resources_info_dict[key2]["Name"]}】在第【{value2[1]}】段时间缺少【{-value2[2]}】'
                        Errors["Value"] = -value2[2]
                    if value2[0] == 'R-5-F':
                        Errors["Tasks"] = [{"Id": _, "Name": task_info_dict[_]["Name"]} for _ in value2[2]]
                        Errors["Resources"] = [{"Id": key2, "Name": resources_info_dict[key2]["Name"]}]
                        Errors[
                            "Name"] = f'独占资源【{resources_info_dict[key2]["Name"]}】在第【{value2[1]}】段时间被【{value2[2]}】共用'
                        Errors["Value"] = ""
                    if value2[0] == 'T-1-F':
                        Errors["Tasks"] = [{"Id": key2, "Name": task_info_dict[key2]["Name"]},
                                           {"Id": value2[1], "Name": task_info_dict[value2[1]]["Name"]}]
                        Errors["Resources"] = []
                        Errors[
                            "Name"] = f'前置【{task_info_dict[key2]["Name"]}】的实际开始时间与后置【{task_info_dict[value2[1]]["Name"]}】的实际开始时间有冲突'
                        Errors["Value"] = value2[2]
                    if value2[0] == 'T-2-F':
                        Errors["Tasks"] = [{"Id": key2, "Name": task_info_dict[key2]["Name"]},
                                           {"Id": value2[1], "Name": task_info_dict[value2[1]]["Name"]}]
                        Errors["Resources"] = []
                        Errors[
                            "Name"] = f'前置【{task_info_dict[key2]["Name"]}】的实际开始时间与后置【{task_info_dict[value2[1]]["Name"]}】的实际开始结束有冲突'
                        Errors["Value"] = value2[2]
                    if value2[0] == 'T-3-F':
                        Errors["Tasks"] = [{"Id": key2, "Name": task_info_dict[key2]["Name"]},
                                           {"Id": value2[1], "Name": task_info_dict[value2[1]]["Name"]}]
                        Errors["Resources"] = []
                        Errors[
                            "Name"] = f'前置【{task_info_dict[key2]["Name"]}】的实际结束时间与后置【{task_info_dict[value2[1]]["Name"]}】的实际开始时间有冲突'
                        Errors["Value"] = value2[2]
                    if value2[0] == 'T-4-F':
                        Errors["Tasks"] = [{"Id": key2, "Name": task_info_dict[key2]["Name"]},
                                           {"Id": value2[1], "Name": task_info_dict[value2[1]]["Name"]}]
                        Errors["Resources"] = []
                        Errors[
                            "Name"] = f'前置【{task_info_dict[key2]["Name"]}】的实际结束时间与后置【{task_info_dict[value2[1]]["Name"]}】的实际结束时间有冲突'
                        Errors["Value"] = value2[2]
                    Errors["Type"] = value2[0]
                    Errors["Suggestion"] = []
                    Errors_list.append(Errors)
    return {"SceneId": data_set["SceneId"], "SceneName": data_set["SceneName"], "ErrorCode": error_code,
            "Information": information, "Values": Values_list, "Errors": Errors_list}
