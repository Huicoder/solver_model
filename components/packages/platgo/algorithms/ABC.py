import numpy as np
from .. import GeneticAlgorithm
from ..Population import Population
from ..utils.fitness_single import fitness_single


class ABC(GeneticAlgorithm):
    type = {
        "n_obj": "single",
        "encoding": "real",
        "special": {"constrained/none", "large/none"},
    }

    def __init__(
        self,
        pop_size,
        options,
        optimization_problem,
        control_cb,
        max_fe=10000,
        name="ABC",
        show_bar=False,
        sim_req_cb=None,
        debug=False,
    ):
        super(ABC, self).__init__(
            pop_size,
            options,
            optimization_problem,
            control_cb,
            max_fe=max_fe,
            name=name,
            show_bar=show_bar,
            sim_req_cb=sim_req_cb,
            debug=debug,
        )
        self.limit = 20

    def run_algorithm(self):
        """
         main function for Different Evolution
         if population is None, generate a new population with N
        :param N: population size
        :param population: population to be optimized
        :return:
        """
        pop = self.problem.init_pop()
        self.cal_obj(pop)
        Limit = np.zeros((1, pop.pop_size))
        his = self.problem.init_pop(0)
        while self.not_terminal(pop):
            Pdec = pop.decs.copy()
            Odec = Pdec + (
                np.random.rand(np.shape(Pdec)[0], np.shape(Pdec)[1]) * 2 - 1
            ) * (
                Pdec
                - Pdec[
                    np.random.randint(
                        0, high=np.shape(Pdec)[0], size=(1, Pdec.shape[0])
                    )[0],
                    :,
                ]
            )
            # Odec =blscores1
            Offspring = Population(decs=Odec)
            self.cal_obj(Offspring)

            replace = fitness_single(pop) > fitness_single(Offspring)
            pop[replace] = Offspring[replace]
            Limit[0][~replace] = Limit[0][~replace] + 1

            Fitness = np.exp(pop.objv / np.mean(np.abs(pop.objv + 1e-6)))

            Q = self.RouletteWheelSelection(pop, Fitness)
            # #
            # Q = blscores2[0]
            # Q=Q-1
            # #
            Q = Q.astype(np.int)
            Pdec = pop.decs.copy()
            Odec = Pdec[Q] + (
                np.random.rand(np.shape(Pdec)[0], np.shape(Pdec)[1]) * 2 - 1
            ) * (
                Pdec[Q]
                - Pdec[
                    np.random.randint(
                        0, high=np.shape(Pdec)[0], size=(1, Pdec.shape[0])
                    )[0],
                    :,
                ]
            )
            # Odec =blscores3
            Offspring = Population(decs=Odec)
            self.cal_obj(Offspring)
            replace = fitness_single(pop) > fitness_single(Offspring)
            pop[replace] = Offspring[replace]
            Limit[0][~replace] = Limit[0][~replace] + 1

            Q = Limit > self.limit

            if Q.any():
                temppop = self.problem.init_pop(sum(Q[0]))
                # pop[Q1] = pg.Population(decs=popq.decs)
                self.cal_obj(temppop)
                pop[Q[0]] = temppop
                Limit[0][Q[0]] = 0
            bestindex = np.argmin(pop.objv)
            if his.pop_size == 0:
                his = pop[np.int(bestindex)]
            else:
                if his.objv > np.min(pop.objv):
                    his = pop[np.int(bestindex)]
                else:
                    rand = np.int(np.random.rand(1) * self.problem.pop_size)
                    pop[rand] = his
        return pop

    def RouletteWheelSelection(self, pop: Population, Fitness):
        # Fitness = np.reshape(Fitness, (-1, 1))
        # Fitness = Fitness - np.min(np.min(Fitness), 0) + 1e-6
        # Fitness = np.cumsum(1.0 / Fitness)
        # Fitness = Fitness / np.max(Fitness)
        FitnessAll = sum(Fitness)
        Fitness = Fitness / FitnessAll
        Fitness = np.cumsum(Fitness)
        Fitness[-1] = 1.0
        index = np.zeros((pop.pop_size), dtype=np.int64)
        for i in range(pop.pop_size):
            rand = np.random.uniform(0, 1)
            for j in range(pop.pop_size):
                if j == 0:
                    if rand < Fitness[j]:
                        index[i] = j
                else:
                    if (rand > Fitness[j - 1]) and (rand < Fitness[j]):
                        index[i] = j

        return index
