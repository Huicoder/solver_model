import numpy as np
import random
from .. import GeneticAlgorithm


class WOA(GeneticAlgorithm):
    type = {
        "n_obj": "single",
        "encoding": "real",
        "special": ""
    }

    def __init__(
        self,
        pop_size,
        options,
        optimization_problem,
        control_cb,
        max_fe=10000,
        name="WOA",
        show_bar=False,
        sim_req_cb=None,
        debug=False,
    ):
        super(WOA, self).__init__(
            pop_size,
            options,
            optimization_problem,
            control_cb,
            max_fe=max_fe,
            name=name,
            show_bar=show_bar,
            sim_req_cb=sim_req_cb,
            debug=debug
        )

    def run_algorithm(self):
        pop = self.problem.init_pop()
        self.cal_obj(pop)
        dim = self.problem.n_var
        X = pop.decs.copy()
        fitness = pop.objv.copy()
        fitness, sortIndex = SortFitness(fitness)
        X = SortPosition(X, sortIndex)
        N = self.problem.pop_size
        while self.not_terminal(pop):
            t = self._gen
            MaxIter = self._max_fe
            Leader = X[0, :]
            a = 2 - t * (2 / MaxIter)  # 线性下降权重2 - 0
            a2 = -1 + t * (-1 / MaxIter)
            for i in range(self.problem.pop_size):
                r1 = random.random()
                r2 = random.random()
                A = 2 * a * r1 - a
                C = 2 * r2
                b = 1
                l = (a2 - 1) * random.random() + 1  # noqa
                for j in range(dim):
                    p = random.random()
                    if p < 0.5:
                        if np.abs(A) >= 1:
                            rand_leader_index = min(
                                int(np.floor(N * random.random() + 1)), N - 1)
                            X_rand = X[rand_leader_index, :]
                            D_X_rand = np.abs(C * X_rand[j] - X[i, j])
                            X[i, j] = X_rand[j] - A * D_X_rand
                        elif np.abs(A) < 1:
                            D_Leader = np.abs(C * Leader[j] - X[i, j])
                            X[i, j] = Leader[j] - A * D_Leader
                    elif p >= 0.5:
                        distance2Leader = np.abs(Leader[j] - X[i, j])
                        X[i, j] = distance2Leader * \
                            np.exp(b * l) * np.cos(l * 2 * np.pi) + Leader[j]
            pop.decs = X
            self.cal_obj(pop)
            X = pop.decs.copy()
            fitness = pop.objv.copy()  # 计算适应度值
            fitness, sortIndex = SortFitness(fitness)  # 对适应度值排序
            X = SortPosition(X, sortIndex)  # 种群排序
            print("done")
        return pop


def SortFitness(Fit):
    fitness = np.sort(Fit, axis=0)
    index = np.argsort(Fit, axis=0)
    return fitness, index


def SortPosition(X, index):
    Xnew = np.zeros(X.shape)
    for i in range(X.shape[0]):
        Xnew[i, :] = X[index[i], :]
    return Xnew
