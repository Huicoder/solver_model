import numpy as np

# import copy
from .. import GeneticAlgorithm
from ..Population import Population
from scipy.spatial import distance


class AFSA(GeneticAlgorithm):
    type = {"n_obj": "single", "encoding": "real", "special": ""}

    def __init__(
        self,
        pop_size,
        options,
        optimization_problem,
        control_cb,
        max_fe=10000,
        name="AFSA",
        show_bar=False,
        sim_req_cb=None,
        debug=False,
    ):
        super(AFSA, self).__init__(
            pop_size,
            options,
            optimization_problem,
            control_cb,
            max_fe=max_fe,
            name=name,
            show_bar=show_bar,
            sim_req_cb=sim_req_cb,
            debug=debug,
        )

    def run_algorithm(self):
        """
         main function for Different Evolution
         if population is None, generate a new population with N
        :param N: population size
        :param population: population to be optimized
        :return:
        """
        GroupFish = self.problem.init_pop()
        self.cal_obj(GroupFish)
        # algo params
        dim = self.problem.n_var
        population = self.problem.pop_size
        # GroupFish=[]
        trytimes = 1
        # Fish params
        Visual = 2
        step = 3

        # params
        # i=0
        # iteration=10

        # StoreBest=[]
        # init Fish
        # initialize(dim, population, GroupFish)

        # B=getBestFish(GroupFish)
        bestindex = np.argmin(GroupFish.objv)

        B = GroupFish[np.int(bestindex)]

        StoreBest = B

        while self.not_terminal(GroupFish):
            j = 0
            while j < population:
                k = 0
                while k < trytimes:
                    temp_Position = self.makeTemp(GroupFish[j], Visual)
                    if GroupFish[j].objv < temp_Position.objv:
                        GroupFish[j] = self.prey(
                            GroupFish[j],
                            temp_Position,
                            B,
                            dim,
                            step,
                            population,
                            Visual,
                            GroupFish,
                            j,
                        )
                        break
                    k = k + 1
                GroupFish[j] = self.moveRandomly(GroupFish[j], Visual)
                j = j + 1
                # leapFish(GroupFish)
            # i=i+1
            bestindex = np.argmin(GroupFish.objv)
            B = GroupFish[np.int(bestindex)]
            # B=getBestFish(GroupFish)
            StoreBest += B
            BEindex = np.argmin(StoreBest.objv)
            BE = StoreBest[np.int(BEindex)]
            rand = np.int(np.random.rand(1) * self.problem.pop_size)
            GroupFish[rand] = BE
            # StoreBest.append(copy.deepcopy(B))

        return GroupFish

    def makeTemp(self, ind, Visual):
        # temp_pos = self.problem.init_pop(0)
        # self.cal_obj(temp_pos)
        for i in range(len(ind.decs[0])):
            tempdec = ind.decs.copy()
            tempdec[0, i] = ind.decs[0, i] + (Visual * np.random.randn())
            tempdec = self.xianjie(tempdec)
            temp_pos = Population(decs=tempdec)
            self.cal_obj(temp_pos)
            # if temp_pos.pop_size==0:
            #     temp_pos=temp
            # else:
            #     temp_pos=temp+temp_pos
        return temp_pos

    def prey(self, ind, TF, B, dim, step, n, Visual, GroupFish, j):
        crowdFactor = np.random.uniform(0.5, 1)
        new_State = []
        for i in range(dim):
            m = ind.decs[0, i] + (
                (
                    (TF.decs[0, i] - ind.decs[0, i])
                    / distance.euclidean(TF.decs, ind.decs)
                )
                * step
                * np.random.randn()
            )
            new_State.append(m)
        new_State = np.array(new_State).reshape(1, -1)
        # new_fit = calcFitness(ind.position)
        new_State = self.xianjie(new_State)
        new_fit = Population(decs=new_State)
        self.cal_obj(new_fit)
        nf = Visual * n

        # For Swarm
        for i in range(int(round(nf))):
            Xc = []
            if j != 0 and j != n - 1:
                # print j
                for x in range(dim):
                    element = (
                        GroupFish[j - 1].decs[0, x]
                        + GroupFish[j].decs[0, x]
                        + GroupFish[j + 1].decs[0, x]
                    )
                    Xc.append(element)
        Xcfitnessdec = np.array(new_State).reshape(1, -1)
        Xcfitnessdec = self.xianjie(Xcfitnessdec)
        Xcfitness = Population(decs=Xcfitnessdec)
        self.cal_obj(Xcfitness)
        # Xcfitness = calcFitness(Xc)  # center position

        # follow
        if B.objv > ind.objv and (nf / n) < crowdFactor:
            ind = self.follow(ind, B, dim, step)

        # swarm
        elif Xcfitness.objv > ind.objv and (nf / n) < crowdFactor:

            ind = self.swarm(ind, TF, dim, step)

        else:
            # ind.position = new_State
            # ind.fitness = new_fit
            ind = new_fit
        # ind1 = self.follow(ind, B, dim, step)
        # ind2 = self.swarm(ind, TF, dim, step)
        # ind3 = new_fit
        # if ind1.objv<=ind2.objv and ind1.objv<=ind3.objv:
        #     ind = ind1
        #     # print(1)
        # elif ind2.objv<=ind1.objv and ind2.objv<=ind3.objv:
        #     ind = ind2
        #     # print(2)
        # elif ind3.objv<=ind1.objv and ind3.objv<=ind2.objv:
        #     ind = ind3
        #     # print(3)
        # k=1

        return ind

    def follow(self, ind, TF, dim, step):
        new_State3 = []
        for i in range(dim):
            L = ind.decs[0, i] + (
                (
                    (TF.decs[0, i] - ind.decs[0, i])
                    / distance.euclidean(TF.decs, ind.decs)
                )
                * step
                * np.random.randn()
            )
            new_State3.append(L)

        new_State3dec = np.array(new_State3).reshape(1, -1)
        new_State3dec = self.xianjie(new_State3dec)
        new_State3pop = Population(decs=new_State3dec)
        self.cal_obj(new_State3pop)
        # ind.position = new_State3
        # ind.fitness = calcFitness(new_State3)
        ind = new_State3pop

        return ind

    def swarm(self, ind, B, dim, step):
        new_State2 = []
        for i in range(dim):
            m = ind.decs[0, i] + (
                (
                    (B.decs[0, i] - ind.decs[0, i])
                    / distance.euclidean(B.decs, ind.decs)
                )
                * step
                * np.random.randn()
            )
            new_State2.append(m)
        new_State2dec = np.array(new_State2).reshape(1, -1)
        new_State2dec = self.xianjie(new_State2dec)
        new_State2pop = Population(decs=new_State2dec)
        self.cal_obj(new_State2pop)
        # ind.position = new_State2
        # ind.fitness = calcFitness(new_State2)
        ind = new_State2pop

        return ind

    def moveRandomly(self, ind, Visual):
        new_State = []
        for i in range(len(ind.decs[0])):
            n = ind.decs[0, i] + (Visual * np.random.randn())
            new_State.append(n)
        new_Statedec = np.array(new_State).reshape(1, -1)
        new_Statedec = self.xianjie(new_Statedec)
        new_Statepop = Population(decs=new_Statedec)
        self.cal_obj(new_Statepop)
        # ind.position = new_State
        # ind.fitness = calcFitness(ind.position)
        ind = new_Statepop

        return ind

    def xianjie(self, dec):
        for m in range(np.shape(dec)[0]):
            for n in range(np.shape(dec)[1]):
                if dec[m, n] > self.problem.ub[0]:
                    dec[m, n] = self.problem.ub[0]
                elif dec[m, n] < self.problem.lb[0]:
                    dec[m, n] = self.problem.lb[0]
                else:
                    dec[m, n] = dec[m, n]
        return dec
