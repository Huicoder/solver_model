import numpy as np
import random
from .. import GeneticAlgorithm, utils, Population
from scipy.optimize import minimize
from scipy.linalg import orth
from scipy.spatial.distance import cdist


class SAMSO(GeneticAlgorithm):
    type = {
        "n_obj": "single",
        "encoding": "real",
        "special": {"large/none", "expensive"}
    }

    def __init__(
        self,
        pop_size,
        options,
        optimization_problem,
        control_cb,
        max_fe=500,
        name="SAMSO",
        show_bar=False,
        sim_req_cb=None,
        debug=False
    ):
        super(SAMSO, self).__init__(
            pop_size,
            options,
            optimization_problem,
            control_cb,
            max_fe=max_fe,
            name=name,
            show_bar=show_bar,
            sim_req_cb=sim_req_cb,
            debug=debug
        )
        self.Wnc = 1
        self.Pr = 0.5
        self.maxFE = 10000
        self.FE = 0

    def run_algorithm(self):
        self.maxFE = self._max_fe
        # self._max_fe = self._max_fe * self.problem.pop_size
        eta = np.min(
            (np.sqrt(0.001 ** 2 * self.problem.n_var), (5e-4) * np.min(np.array(self.problem.ub) - np.array(self.problem.lb))))  # noqa
        if self.problem.n_var > 50:
            N = 80
            K = 2 * self.problem.n_var
        else:
            N = 40
            K = N
        PopDec, _ = utils.uniform_point(K, self.problem.n_var, "Latin")
        dec = np.tile(np.array(self.problem.ub) - np.array(self.problem.lb), (K, 1)) * PopDec + np.tile(  # noqa
            self.problem.lb, (K, 1))

        pop = Population(decs=dec)
        self.cal_obj(pop)
        # self._gen = len(pop)
        # print(self._gen)
        self.FE += dec.shape[0]
        idx = np.argsort(pop.objv, axis=0)
        Select = idx[0:N]
        Position = np.hstack((pop[Select].decs, pop[Select].objv))
        Vmax = 0.5 * (np.array(self.problem.ub) - np.array(self.problem.lb))
        Vmin = -0.5 * Vmax
        Velocity = np.random.random(size=(
            N, self.problem.n_var)) * np.tile(Vmax - Vmin, (N, 1)) + np.tile(Vmin, (N, 1))  # noqa
        Pbest = Position.copy()
        Gbest = Position[0, :].copy()
        maxFES = self.maxFE - K
        while self.not_terminal(pop):
            # self._gen -= 1
            Gbest = Gbest.flatten()
            model = rbf_build(pop.decs, pop.objv)
            srgtMin = np.array(
                [FindOpt(model, pop, np.array(self.problem.ub), np.array(self.problem.lb))])  # noqa
            dist = cdist(pop.decs, srgtMin)
            dxRBF = np.min(dist)
            if dxRBF > eta:
                optSrgt = Population(decs=srgtMin)
                self.cal_obj(optSrgt)
                # self._gen += len(optSrgt)
                self.FE += 1
                pop = pop + optSrgt
                if optSrgt.objv < Gbest[-1]:
                    model = rbf_build(pop.decs, pop.objv)
                    Gbest = np.hstack((optSrgt.decs, optSrgt.objv)).flatten()
            currFES = self.FE - K
            pop, Position, Velocity, Gbest, Pbest, self.FE = self.UpdatePosition(pop, Position, Velocity, Pbest, Gbest,  # noqa
                                                                                currFES, maxFES, self.Wnc, self.Pr, model,  # noqa
                                                                                 eta, self.FE)  # noqa
            # print(np.min(pop.objv))
            # print(self._gen)
        return pop

    def UpdatePosition(self, pop, Position, Velocity, Pbest, Gbest, currFES, maxFES, Wnc, Pr, model, eta, FE):  # noqa
        N, D = Velocity.shape
        N2 = np.min((N, 2 + np.ceil(N * ((maxFES - currFES) / maxFES) ** Wnc)))  # noqa
        N1 = N - N2
        tVelocity = Velocity.copy()
        tPosition = Position.copy()
        if N1 != 0:
            IDX = np.arange(N1).astype(int)
            label = np.array(np.random.random(
                size=(int(N1), 1)) < Pr).flatten()
            w = 0.792
            c1 = 1.491
            c2 = 1.491
            r1 = np.tile(np.random.random(size=(np.sum(label), 1)), (1, D))  # noqa
            r2 = np.tile(np.random.random(size=(np.sum(label), 1)), (1, D))  # noqa
            if IDX[label].size > 0:
                tVelocity[IDX[label].astype(int), :] = w * Velocity[IDX[label], :] + c1 * r1 * (  # noqa
                    Pbest[IDX[label], 0: D] - Position[IDX[label], 0: D]) + \
                   c2 * r2 * (np.tile(Gbest[0: D], (np.sum(label), 1)) - Position[  # noqa
                        IDX[label],
                        0: D])
                tPosition[IDX[label].astype(
                    int), 0: D] = Position[IDX[label], 0: D] + tVelocity[IDX[label], :]  # noqa
            I = np.argsort(pop.objv, axis=0)  # noqa
            num = np.min((len(pop.objv), 2 * D))
            TopDec = pop[I[0: num]].decs
            B = orth(np.cov(TopDec, rowvar=False))
            remain = np.where(~label)[0]
            if remain.size != 0:
                num = len(remain)
                R1 = np.random.random(size=(1, num))
                R2 = np.random.random(size=(1, num))
                for i in range(num):
                    t = np.array(
                        [np.dot(c1 * (Pbest[IDX[remain[i]], 0: D] - Position[IDX[remain[i]], 0: D]), B)])  # noqa
                    t1 = np.array(
                        [np.dot(c1 * (Gbest[0: D] - Position[IDX[remain[i]], 0: D]), B)])  # noqa
                    tVelocity[IDX[remain[i]], :] = w * Velocity[IDX[remain[i]], :] + np.dot(t * R1[0][i], B.T) + \
                        np.dot(t1 * R2[0][i], B.T)  # noqa

        learner1 = np.arange(N2)
        learner2 = random.sample(range(int(N2)), int(N2))
        learner2 = np.array(learner2)
        while np.any(learner1 == learner2):
            replace = learner1 == learner2
            learner2[replace] = random.sample(range(int(N2)), np.sum(replace))  # noqa
        delta = Position[(N1 + learner1).astype(int), 0: D] - \
            Position[(N1 + learner2).astype(int), 0: D]  # noqa
        change = Position[(N1 + learner1).astype(int),
                          D] >= Position[(N1 + learner2).astype(int), D]  # noqa
        delta[change, 0] = -delta[change, 0]
        tPosition[int(N1):, 0: D] = Position[int(N1):, 0: D] + \
            np.random.random(size=(int(N2), 1)) * delta  # noqa
        srgtObj = rbf_predict(tPosition[:, 0: D], model, pop[0: model.n].decs)  # noqa
        dist = np.min(cdist(tPosition[:, 0: D], pop.decs), axis=1)
        evaluation = np.argwhere(np.logical_and(
            dist > eta, srgtObj < Position[:, D]))
        if evaluation.size > 0:
            news = Population(decs=tPosition[evaluation.flatten(), 0: D])  # noqa
            self.cal_obj(news)
            # self._gen += len(news)
            FE += len(news)
            tPosition[evaluation.flatten(), D] = news.objv.flatten()  # noqa
            update = tPosition[evaluation.flatten(
            ), D] < Position[evaluation.flatten(), D]
            Position[evaluation[update].flatten(
            ), :] = tPosition[evaluation[update].flatten(), :]
            Velocity[evaluation[update].flatten(
            ), :] = tVelocity[evaluation[update].flatten(), :]
            Pbest[evaluation[update].flatten(
            ), :] = tPosition[evaluation[update].flatten(), :]
            if np.all(Pbest[evaluation[update].flatten(), D] < Gbest[D]) and (  # noqa
                    Pbest[evaluation[update].flatten(), D]).size > 0:  # noqa
                tPbest = Pbest[evaluation[update].flatten(), :]
                best = np.argmin(tPbest[D])
                Gbest = tPbest[best, :]
            pop = pop + news
        else:
            best = np.argmin(tPosition[:, D])
            news = Population(decs=np.array([tPosition[best, 0: D]]))  # noqa
            self.cal_obj(news)
            # self._gen += len(news)
            FE += len(news)
            pop = pop + news
            if news.objv < Position[best, D]:
                Position[best, 0: D] = tPosition[best, 0: D]
                Position[best, D] = news.objv
                Velocity[best, :] = tVelocity[best, :]
                Pbest[best, :] = Position[best, :]
                if news.objv < Gbest[D]:
                    Gbest = Position[best, :]
        return pop, Position, Velocity, Gbest, Pbest, FE


def rbf_predict(Xq, model, Xtr, *args):
    if model.n != Xtr.shape[0]:
        assert 0, "The matrix Xtr should be the same matrix with which the model was built."  # noqa
    if Xq.ndim == 1:
        Xq = np.array([Xq])
    nq = Xq.shape[0]
    Yq = np.zeros(shape=(nq, 1))
    for t in range(nq):
        if model.bf_type == "MQ":
            dist = np.sqrt(
                np.sum((np.tile(Xq[t, :], (model.n, 1)) - Xtr) ** 2, axis=1, keepdims=True) + model.bf_c ** 2)  # noqa
        if model.poly != 0:
            Yq[t] = np.dot(model.coefs.T, np.vstack(
                (dist, [1], np.array([Xq[t, :]]).T)))  # noqa
    if len(args) != 0:
        BD = args[0]
        BU = args[1]
        Xq = np.clip(Xq, BU, BD)
    return Yq.flatten()


def FindOpt(model, pop, BU, BD):
    I = np.argsort(pop.objv, axis=0)  # noqa
    init = 0
    preObj = rbf_predict(pop[I[init]].decs, model, pop.decs)  # noqa
    while preObj == np.inf:
        init += 1
        preObj = rbf_predict(model, pop.decs, pop[I[init]].decs)
    x = pop[I[init]].decs
    bnds = np.vstack((BD, BU)).T.astype(int)
    try:
        opt = minimize(rbf_predict, np.clip(x, BD, BU), args=(
            model, pop.decs, BD, BU), method='SLSQP', bounds=bnds)  # noqa
    except:  # noqa
        return np.random.uniform(BD[0], BU[0], size=x.shape[1])
    return opt["x"]


def rbf_build(*args):  # Xtr, Ytr, bf_type, bf_c, usePolyPart, verbose
    if len(args) < 2:
        assert 0, "Too few input arguments"
    else:
        Xtr = args[0]
        Ytr = args[1]
        n, d = Xtr.shape
        ny, dy = Ytr.shape
        if n < 2 or d < 1 or ny != n or dy != 1:
            assert 0, "Wrong training data sizes."
        if len(args) < 3:
            bf_type = "MQ"
        if len(args) < 4:
            bf_c = 1
        if len(args) < 5:
            usePolyPart = 1
        if len(args) < 6:
            verbose = 0
        model = Model(n, np.mean(Ytr), bf_type, bf_c, usePolyPart)
        dist = np.zeros(shape=(n, n))
        if model.bf_type == "MQ":
            if verbose:
                print("Building RBF (multiquadric) model...")
            for i in range(n):
                dist[i, i: n] = np.sqrt(
                    np.sum((np.tile(Xtr[i, :], (n - i, 1)) - Xtr[i: n, :]) ** 2, axis=1) + bf_c ** 2)  # noqa
                dist[i + 1: n, i] = dist[i, i + 1: n]
        if model.poly == 0:
            model.coefs = np.dot(np.linalg.inv(dist), (Ytr - model.meanY))  # noqa
        else:
            tmp = np.hstack((dist, np.ones(shape=(n, 1)), Xtr))
            tmp1 = np.hstack(
                (np.hstack((np.ones(shape=(n, 1)), Xtr)).T, np.zeros(shape=(d + 1, d + 1))))  # noqa
            A = np.vstack((tmp, tmp1))
            tmp2 = np.vstack((Ytr, np.zeros(shape=(d + 1, 1))))
            try:
                model.coefs = np.dot(np.linalg.inv(A), tmp2)
            except:  # noqa
                print("警告: 矩阵接近奇异值，或者缩放错误。结果可能不准确")
                model.coefs = np.dot(np.linalg.pinv(A), tmp2)
    return model


class Model:
    def __init__(self, n, meanY, bf_type, bf_c, poly, coefs=None):
        self.n = n
        self.meanY = meanY
        self.bf_type = bf_type
        self.bf_c = bf_c
        self.poly = poly
        self.coefs = coefs
