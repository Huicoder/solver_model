from interface.Conflict import Conflict_T
import datetime
def get_time_by_type(task_id, time_type, task_info_dict):
    """
    根据枚举类型取出对应的时间类型
    :param task_id: 任务唯一标识
    :param time_type: 枚举类型
    :param task_info_dict: 存储任务信息字典
    :return: 时间类型
    """
    if time_type == 0:  # 实际开始时间
        time = task_info_dict[task_id]["RealStart"]
        description = "实际开始时间"
        relative_time = task_info_dict[task_id]["RealFinish"]
    elif time_type == 1:  # 实际结束时间
        time = task_info_dict[task_id]["RealFinish"]
        description = "实际结束时间"
        relative_time = task_info_dict[task_id]["RealStart"]
    elif time_type == 2:  # 最早开始时间
        time = task_info_dict[task_id]["EarlyStartDate"]
        description = "最早开始时间"
        relative_time = task_info_dict[task_id]["LateFinishDate"]
    elif time_type == 3:  # 最迟结束时间
        time = task_info_dict[task_id]["LateFinishDate"]
        description = "最迟结束时间"
        relative_time = task_info_dict[task_id]["EarlyStartDate"]
    elif time_type == 4:  # 期望开始时间
        time = task_info_dict[task_id]["ExpectStartTime"]
        description = "期望开始时间"
        if "ExpectFinishTime" in task_info_dict[task_id]:
            relative_time = task_info_dict[task_id]["ExpectFinishTime"]
        else:
            relative_time = (datetime.datetime.strptime(time, "%Y-%m-%dT%H:%M:%S") + datetime.timedelta(
                seconds=task_info_dict[task_id]["NeedTimes"])).strftime("%Y-%m-%dT%H:%M:%S")
    else:  # 期望结束时间
        time = task_info_dict[task_id]["ExpectFinishTime"]
        description = "期望结束时间"
        if "ExpectStartTime" in task_info_dict[task_id]:
            relative_time = task_info_dict[task_id]["ExpectStartTime"]
        else:
            relative_time = (datetime.datetime.strptime(time, "%Y-%m-%dT%H:%M:%S") - datetime.timedelta(
                seconds=task_info_dict[task_id]["NeedTimes"])).strftime("%Y-%m-%dT%H:%M:%S")
    return description, time, relative_time


def get_type(description, description2):
    """
    获取冲突类型值
    :param description:
    :param description2:
    :return:
    """
    if description == Conflict_T.EXPECT_START_TIME.value and description2 == Conflict_T.EXPECT_START_TIME.value:
        return "T-1"
    if description == Conflict_T.EXPECT_START_TIME.value and description2 == Conflict_T.EXPECT_FINISH_TIME.value:
        return "T-1"
    if description == Conflict_T.EXPECT_FINISH_TIME.value and description2 == Conflict_T.EXPECT_START_TIME.value:
        return "T-3"
    if description == Conflict_T.EXPECT_FINISH_TIME.value and description2 == Conflict_T.EXPECT_FINISH_TIME.value:
        return "T-4"
    if description == Conflict_T.REAL_START.value and description2 == Conflict_T.REAL_START.value:
        return "T-1-F"
    if description == Conflict_T.REAL_START.value and description2 == Conflict_T.REAL_FINISH.value:
        return "T-2-F"
    if description == Conflict_T.REAL_FINISH.value and description2 == Conflict_T.REAL_START.value:
        return "T-3-F"
    if description == Conflict_T.REAL_FINISH.value and description2 == Conflict_T.REAL_FINISH.value:
        return "T-4-F"


def get_relative_description(description):
    """
    得到对应的时间类型描述
    :param description: 原始的时间类型描述
    :return:
    """
    relative_description_dict = dict()
    relative_description_dict[Conflict_T.EARLY_START_DATE.value] = f'{Conflict_T.EARLY_START_DATE.value+","+Conflict_T.LATE_FINISH_DATE.value}'
    relative_description_dict[Conflict_T.LATE_FINISH_DATE.value] = f'{Conflict_T.EARLY_START_DATE.value+","+Conflict_T.LATE_FINISH_DATE.value}'

    relative_description_dict[Conflict_T.EXPECT_START_TIME.value] = f'{Conflict_T.EXPECT_START_TIME.value+","+Conflict_T.EXPECT_FINISH_TIME.value}'
    relative_description_dict[Conflict_T.EXPECT_FINISH_TIME.value] = f'{Conflict_T.EXPECT_START_TIME.value+","+Conflict_T.EXPECT_FINISH_TIME.value}'

    relative_description_dict[Conflict_T.REAL_START.value] = f'{Conflict_T.REAL_START.value+","+Conflict_T.REAL_FINISH.value}'
    relative_description_dict[Conflict_T.REAL_FINISH.value] = f'{Conflict_T.REAL_START.value+","+Conflict_T.REAL_FINISH.value}'
    return relative_description_dict[description]

def get_is_intersection(a, b, c):
    a = [(datetime.datetime.strptime(_, "%Y-%m-%dT%H:%M:%S") + datetime.timedelta(
        seconds=c)).strftime("%Y-%m-%dT%H:%M:%S") for _ in a]
    if a[0] >= b[0] and a[1] <= b[1]:
        return True
    else:
        return False
def get_suggestion(operator_str, description, description2, value, real_value, before_task_name, after_task_name,
                   task_info_dict, before_task_id, after_task_id, before_time_list, after_time_list):
    """
    给出冲突类型的建议
    :param after_time_list:
    :param before_time_list:
    :param after_task_id:
    :param before_task_id:
    :param task_info_dict:
    :param after_task_name:
    :param before_task_name:
    :param real_value:
    :param value:
    :param operator_str:
    :param description:
    :param description2:
    :return:
    """
    method = list()
    before_time_list = sorted(before_time_list)
    after_time_list = sorted(after_time_list)
    before_task_early_start_date = task_info_dict[before_task_id]["EarlyStartDate"]
    before_task_late_finish_date = task_info_dict[before_task_id]["LateFinishDate"]
    after_task_early_start_date = task_info_dict[after_task_id]["EarlyStartDate"]
    after_task_late_finish_date = task_info_dict[after_task_id]["LateFinishDate"]
    d = value - real_value
    if operator_str == "!=":
        flag1 = get_is_intersection(after_time_list, [after_task_early_start_date, after_task_late_finish_date], d)
        flag2 = get_is_intersection(before_time_list, [before_task_early_start_date, before_task_late_finish_date], -d)
        if d > 0:
            if flag1:
                method.append(
                    f"后置【{after_task_name}】的【{get_relative_description(description2)}】整体后移{d}个时间单位")
            if flag2:
                method.append(
                    f"前置【{before_task_name}】的【{get_relative_description(description)}】整体前移{d}个时间单位")
        else:
            if flag1:
                method.append(
                    f"后置【{after_task_name}】的【{get_relative_description(description2)}】整体前移{-d}个时间单位")
            if flag2:
                method.append(
                    f"前置【{before_task_name}】的【{get_relative_description(description)}】整体后移{-d}个时间单位")
    elif operator_str == "<=":
        flag1 = get_is_intersection(after_time_list, [after_task_early_start_date, after_task_late_finish_date], d + 1)
        flag2 = get_is_intersection(before_time_list, [before_task_early_start_date, before_task_late_finish_date],
                                    -(d + 1))
        if flag1:
            method.append(
                f"后置【{after_task_name}】的【{get_relative_description(description2)}】至少整体后移{d + 1}个时间单位")
        if flag2:
            method.append(
                f"前置【{before_task_name}】的【{get_relative_description(description)}】至少整体前移{d + 1}个时间单位")
    elif operator_str == "<":
        flag1 = get_is_intersection(after_time_list, [after_task_early_start_date, after_task_late_finish_date], d)
        flag2 = get_is_intersection(before_time_list, [before_task_early_start_date, before_task_late_finish_date], -d)
        if flag1:
            method.append(
                f"后置【{after_task_name}】的【{get_relative_description(description2)}】至少整体后移{d}个时间单位")
        if flag2:
            method.append(
                f"前置【{before_task_name}】的【{get_relative_description(description)}】至少整体前移{d}个时间单位")
    elif operator_str == ">=":
        flag1 = get_is_intersection(after_time_list, [after_task_early_start_date, after_task_late_finish_date],
                                    -(d + 1))
        flag2 = get_is_intersection(before_time_list, [before_task_early_start_date, before_task_late_finish_date],
                                    d + 1)
        if flag1:
            method.append(
                "后置【{after_task_name}】的【{get_relative_description(description2)}】至少整体前移{d + 1}个时间单位")
        if flag2:
            method.append(
                f"前置【{before_task_name}】的【{get_relative_description(description)}】至少整体后移{d + 1}个时间单位")
    else:
        flag1 = get_is_intersection(after_time_list, [after_task_early_start_date, after_task_late_finish_date], d)
        flag2 = get_is_intersection(before_time_list, [before_task_early_start_date, before_task_late_finish_date], -d)
        if flag1:
            method.append(
                "后置【{after_task_name}】的【{get_relative_description(description2)}】至少整体前移{-d}个时间单位")
        if flag2:
            method.append(
                f"前置【{before_task_name}】的【{get_relative_description(description)}】至少整体后移{-d}个时间单位")
    if not flag1 and not flag2:
        method.append(f"删除前置【{before_task_name}】或者后置【{after_task_name}】")
    return method
